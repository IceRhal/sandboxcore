package net.IceRhal.SandboxCore.Chat;

public class Modifier {

	protected ModifierType modifierType;
	protected String value;
	
	public Modifier(ModifierType type, String str) {
		
		modifierType = type;
		value = str;
	}
	
	public String getValue() {
		
		return value;
	}
	
	public ModifierType getType() {
		
		return modifierType;
	}
}