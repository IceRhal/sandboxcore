package net.IceRhal.SandboxCore.Chat;

public class ClickModifier extends Modifier {

	static ModifierType modifierType = ModifierType.CLICK_EVENT;
	
	private ClickModifier(ModifierType type, String str) {
		
		super(modifierType, str);
	}	
	
	public ClickModifier(ClickAction action, String str) {
	
		super(modifierType, str);
		
		switch(action) {
		
		case OPEN_URL: {
		
			this.value = "action':'open_url', 'value':'" + str.replace("'", "//'");   
			break;
		}
		
		case RUN_COMMAND: {
			
			this.value = "action':'run_command', 'value':'" + str.replace("'", "//'");
			break;
		}
		
		case SUGGEST_COMMAND: {
			
			this.value = "action':'suggest_command', 'value':'" + str.replace("'", "//'");
			break;
		}
		
		default : return;		
		
		}
	}
}
