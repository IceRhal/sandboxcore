package net.IceRhal.SandboxCore.Chat;

public enum ModifierType {
	
	COLOR,
	
	BOLD,
	
	UNDERLINED,
	
	ITALIC,
	
	STRIKETHROUGH,
	
	OBFUSCATED,
	
	INSERTION,
	
	CLICK_EVENT,
	
	HOVER_EVENT;
}
