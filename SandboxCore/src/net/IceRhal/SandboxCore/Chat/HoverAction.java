package net.IceRhal.SandboxCore.Chat;

public enum HoverAction {

	SHOW_TEXT,
		
	SHOW_ITEM,
	
	SHOW_ACHIEVEMENT,
	
	SHOW_ENTITY;	
}
