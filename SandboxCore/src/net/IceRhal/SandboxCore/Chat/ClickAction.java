package net.IceRhal.SandboxCore.Chat;

public enum ClickAction {

	OPEN_URL,
	
	RUN_COMMAND,
	
	SUGGEST_COMMAND;	
}