package net.IceRhal.SandboxCore.Chat;

public class HoverModifier extends Modifier {

	static ModifierType modifierType = ModifierType.HOVER_EVENT;
	
	private HoverModifier(ModifierType type, String str) {
		
		super(modifierType, str);
	}
	
	public HoverModifier(HoverAction action, String str) {
		
		super(modifierType, str);
		
		switch(action) {
		
		case SHOW_ACHIEVEMENT: {
		
			this.value = "action':'show_achievement', 'value':'" + str.replace("'", "//'");   
			break;
		}
		
		case SHOW_ENTITY: {
			
			this.value = "action':'show_entity', 'value':'" + str.replace("'", "//'");
			break;
		}
		
		case SHOW_ITEM: {
			
			this.value = "action':'show_item', 'value':'" + str.replace("'", "//'");
			break;
		}		
		
		case SHOW_TEXT: {
			
			this.value = "action':'show_text', 'value':'" + str.replace("'", "//'");
			break;
		}
		
		default : return;		
		
		}
	}

}
