package net.IceRhal.SandboxCore.Chat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class CustomChar {

	ArrayList<CustomString> customStrings = new ArrayList<CustomString>();
	
	public CustomChar(CustomString str) {		
		
		addCustomString(str);
	}
	
	public CustomChar(CustomString str1, CustomString str2) {
		
		addCustomString(str1);
		addCustomString(str2);
	}
	
	public CustomChar concat(CustomString str) {
		
		addCustomString(str);
		
		return this;
	}
	
	public CustomString[] getCustomString() {
		
		return customStrings.toArray(new CustomString[customStrings.size()]);	
	}
	
	protected void addCustomString(CustomString str) {
		
		String text = str.getString();
		
		Iterator<String> iterator = Arrays.asList(text.split(" ")).iterator();
		text = "";
		
		String regex = "((http:\\/\\/|https:\\/\\/|www\\.)(www.)?(([a-zA-Z0-9-]){2,}\\.)" +
				"{1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)";
		
		while(iterator.hasNext()) {
			
			String w = iterator.next();
			
			if(w.matches(regex)) {
								
				if(!w.toLowerCase().startsWith("http")) w = "http://" + w;			

				customStrings.add(new CustomString(text).setModifiers(str.getModifier()));
				customStrings.add(new CustomString(w).setModifiers(str.getModifier())
						.addModifier(new ClickModifier(ClickAction.OPEN_URL, w.replace(" ", ""))));
				
				text = "";
			}
			else text += w;
						
			if(iterator.hasNext() || str.getString().endsWith(text + " ")) text += " ";
		}
		
		customStrings.add(new CustomString(text).setModifiers(str.getModifier()));
	}
}
