package net.IceRhal.SandboxCore.Chat;

import java.util.ArrayList;
import java.util.Arrays;


public class CustomString {

	ArrayList<Modifier> modifier = new ArrayList<Modifier>();
	String string;
	
	public CustomString(String str) {
		
		string = str;
	}
	
	public CustomString(Modifier mod, String str) {
		
		modifier.add(mod);
		
		string = str;
	}
	
	public CustomString(ArrayList<Modifier> mod, String str) {
		
		modifier = mod;
		string = str;
	}
	
	public CustomString addModifier(Modifier mod) {
		
		modifier.add(mod);
		
		return this;
	}
	
	public String getString() {
		
		return string;
	}
	
	public Modifier[] getModifier() {
		
		return modifier.toArray(new Modifier[modifier.size()]);
	}
	
	public CustomString setModifiers(Modifier[] modifiers) {
		
		modifier = new ArrayList<Modifier>(Arrays.asList(modifiers));
		
		return this;
	}
}
