package net.IceRhal.SandboxCore.Chat;

import java.util.Arrays;
import java.util.Iterator;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;

public class ChatAssembler {	
	
	String serialize;	
	
	public ChatAssembler(CustomString str) {
		
		serialize = getSerialize(str);
	}
	
	public ChatAssembler(CustomChar chr) {
		
		Iterator<CustomString> it = Arrays.asList(chr.getCustomString()).iterator();
		
		serialize = "[{'text':'', 'extra':[";
		
		while(it.hasNext()) {
			
			CustomString str = it.next();
			
			if(it.hasNext()) serialize += getSerialize(str) + ", ";
			else serialize += getSerialize(str);
		}
		
		serialize += "]}]";		
	}
	
	public String getSerialize() {
		
		return serialize;
	}
	
	public PacketPlayOutChat getPacket() {
		
		return new PacketPlayOutChat(ChatSerializer.a(serialize));
	}	
	
	public ChatAssembler send(Player p) {
		
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(this.getPacket());
		
		return this;
	}
	
	protected static String getSerialize(CustomString str) {
		
		String serialize = "{'text':'" + str.getString().replace("'", "\\'") + "'";
		
		Iterator<Modifier> it = Arrays.asList(str.getModifier()).iterator();

		while(it.hasNext()) {
			
			Modifier mod = it.next();
			ModifierType type = mod.getType();
			
			switch(type) {
			
				case COLOR: {
					
					serialize += ", 'color':'" + mod.getValue() + "'";
					break;
				}
				
				case BOLD: {
					
					if(mod.getValue().equalsIgnoreCase("true")) {
						
						serialize += ", 'bold':'true'";
					}
					else serialize += ", 'bold':'false'";
					
					break;
				}
								
				case UNDERLINED: {
					
					if(mod.getValue().equalsIgnoreCase("true")) {
						
						serialize += ", 'underlined':'true'";
					}
					else serialize += ", 'underlined':'false'";
					
					break;
				}
				
				case ITALIC: {
					
					if(mod.getValue().equalsIgnoreCase("true")) {
						
						serialize += ", 'italic':'true'";
					}
					else serialize += ", 'italic':'false'";
										
					break;
				}
				
				case STRIKETHROUGH: {
					
					if(mod.getValue().equalsIgnoreCase("true")) {
						
						serialize += ", 'strikethrough':'true'";
					}
					else serialize += ", 'strikethrough':'false'";
					
					break;
				}
				
				case OBFUSCATED: {
					
					if(mod.getValue().equalsIgnoreCase("true")) {
						
						serialize += ", 'obfuscated':'true'";
					}
					else serialize += ", 'obfuscated':'false'";
					
					break;
				}
				
				case INSERTION: {
					
					serialize += ", 'insertion':'" + mod.getValue().replace("'", "\\'") + "'"; 
					
					break;
				}
				
				case CLICK_EVENT: {
					
					serialize += ", 'clickEvent':{'" + mod.getValue() + "'}";
					
					break;
				}
				
				case HOVER_EVENT: {
					
					serialize += ", 'hoverEvent':{'" + mod.getValue() + "'}";
					
					break;
				}
				
				default: break;			
			}
		}
		
		serialize += "}";
		
		return serialize;
	}	
}
