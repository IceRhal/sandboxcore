package net.IceRhal.SandboxCore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class Tp implements Listener {
	
	@EventHandler
	public void PlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
		
		if((e.getMessage().toLowerCase().startsWith("/tp ") || e.getMessage().toLowerCase().startsWith("/bukkit:tp ") 
				|| e.getMessage().toLowerCase().startsWith("/minecraft:tp ")) && 
				(e.getPlayer().hasPermission("bukkit.command.tp") || e.getPlayer().hasPermission("minecraft.command.tp"))) {
			
			Bukkit.broadcast(ChatColor.GOLD + e.getPlayer().getName() + ChatColor.YELLOW + " a fait : " + e.getMessage(), "sandboxcore.tp.broadcast");
		}
	}

}
