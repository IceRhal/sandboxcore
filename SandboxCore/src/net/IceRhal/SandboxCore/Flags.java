package net.IceRhal.SandboxCore;

import java.util.Iterator;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Flags implements Listener {
	
	public static StateFlag doorsGrief = new StateFlag("doors-grief", true);
	public static StateFlag entityGrief = new StateFlag("entity-explosion-grief", true);
	public static StateFlag creeperGrief = new StateFlag("creeper-grief", true);
	
	@EventHandler
	public void EntityBreakDoor(EntityBreakDoorEvent e) {
		
		Location l = e.getBlock().getLocation();
		
		Iterator<ProtectedRegion> it = WGBukkit.getRegionManager(l.getWorld()).getApplicableRegions(l).getRegions().iterator();
		
		ProtectedRegion r = null;
		State grief = State.ALLOW;
		
		while(it.hasNext()) {
			
			ProtectedRegion reg = it.next();
			
			if(reg.getFlags().containsKey(doorsGrief) && (r == null || r.getPriority() < reg.getPriority())) grief = reg.getFlag(doorsGrief); 
		}
		
		if(grief == State.DENY) e.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void EntityExplode(EntityExplodeEvent e) {
		
		Location l = e.getLocation();
				
		if(!WGBukkit.getRegionManager(l.getWorld()).getApplicableRegions(l).allows(entityGrief) || 
				(!WGBukkit.getRegionManager(l.getWorld()).getApplicableRegions(l).allows(creeperGrief) 
						&& e.getEntityType() == EntityType.CREEPER)) {
			
			e.blockList().clear();
		}
	}	
	
	static void load() {
		
		Main.addFlags(doorsGrief);
		Main.addFlags(entityGrief);
		Main.addFlags(creeperGrief);
	}
}
