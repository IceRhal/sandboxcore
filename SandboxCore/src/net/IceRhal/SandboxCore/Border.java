package net.IceRhal.SandboxCore;

import java.util.Random;
import java.util.UUID;

import net.minecraft.server.v1_8_R1.ChunkProviderServer;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.World;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.generator.BlockPopulator;

public class Border implements CommandExecutor, Listener {
		
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(label.equalsIgnoreCase("setborder")) {
				
				if(!Main.hasPerm(p, "sandboxcore.setborder")) return true;
				
				if(args.length < 1) {
					
					p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/setborder [rayon]");
					return true;
				}
				
				int nb;
				
				try {
					
					nb = Integer.parseInt(args[0]);
					
					if(nb <= 0) {
						
						p.sendMessage(ChatColor.RED + "Le rayon doit être supérieur à 1.");
					}
				} catch(NumberFormatException e) {
					
					p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/setborder [rayon]");
					return true;
				}
				
				EntityPlayer p2 = ((CraftPlayer)p).getHandle();
								
				World w = p2.world;
								
				w.af().c(w.getSpawn().getX(), w.getSpawn().getZ());
				w.af().a(w.af().h(), nb * 2, 0);
								
				for(Player p3 : Bukkit.getOnlinePlayers()) {
					
					if(check(p3)) {
						
						p3.teleport(p3.getWorld().getSpawnLocation(), TeleportCause.PLUGIN);
						p3.getWorld().playSound(p3.getWorld().getSpawnLocation(), Sound.ENDERMAN_TELEPORT, 100, 1);
					}
				}
				
				p.sendMessage(ChatColor.GREEN + "Bordure de " + ChatColor.DARK_GREEN + nb + ChatColor.GREEN + " de rayon définie.");
			}
			else if(label.equalsIgnoreCase("genborder")) {
							
				/*if(!Main.hasPerm(p, "sandboxcore.genborder")) return true;

				int chunks = (int) ((CraftWorld)p.getWorld()).getHandle().af().h() / 16 + 1;
				
				genX(chunks, chunks, chunks, p.getWorld(), 0, chunks * chunks, p.getUniqueId());
								
				p.sendMessage(ChatColor.GREEN + "Génération du monde " + ChatColor.DARK_GREEN + p.getWorld().getName() +
						ChatColor.GREEN + " en démarrée (" + ChatColor.DARK_GREEN + chunks * chunks + ChatColor.GREEN + " chunks).");
				*/
			}
			else p.sendMessage(ChatColor.RED + "Commande inconnue.");
			
			return true;
		}
		else sender.sendMessage("Cette commande doit etre faites par un joueur.");
		
		return false;
	}
	
	static public boolean check(Player p) {
		
		World w = ((CraftPlayer)p).getHandle().world;
		
		return !w.a(w.af(), ((CraftPlayer)p).getHandle());
	}
	
	static public void genX(final int x, final int z, final int Z, final org.bukkit.World w, int actu, final int max, final UUID id) {
		
		final int act = actu + 1;
		
		Player p = Bukkit.getPlayer(id);
		
		if(p != null) {			

			if(act == max/10 || act == max/10*2 || act == max/10*3 || act == max/10*4 || act == max/10*5
					|| act == max/10*6|| act == max/10*7 || act == max/10*8 || act == max/10*9 || act == max/10*10) {
				
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Génération des chunks du monde " + ChatColor.DARK_PURPLE 
						+ w.getName() + ChatColor.LIGHT_PURPLE + " à " + ChatColor.DARK_PURPLE + ((int) act * 100 / max + 1) + ChatColor.LIGHT_PURPLE + "%");
			}
			
			if(act == max) {
				
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Génération des chunks du monde " + ChatColor.DARK_PURPLE 
						+ w.getName() + ChatColor.LIGHT_PURPLE + " finit.");
				p.performCommand("dynmap fullrender");
			}
		}
		
		if(act >= max) return;
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {

			@Override
			public void run() {
				
				Location l = w.getSpawnLocation();
				
				final Chunk c1 = w.getChunkAt(x + l.getChunk().getX(), z + l.getChunk().getZ());
				final Chunk c2 = w.getChunkAt(x + l.getChunk().getX(), -z + l.getChunk().getZ());
				final Chunk c3 = w.getChunkAt(-x + l.getChunk().getX(), z + l.getChunk().getZ());
				final Chunk c4 = w.getChunkAt(-x + l.getChunk().getX(), -z + l.getChunk().getZ());
				
				ChunkProviderServer cPS = ((CraftWorld)w).getHandle().chunkProviderServer;
				
				if(!c1.isLoaded()) {

					c1.load(true);
					cPS.getChunkAt(cPS, c1.getX(), c1.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c1);
					}
					
					w.unloadChunkRequest(c1.getX(), c1.getZ(), true);
				}				
				if(!c2.isLoaded()) {

					c2.load(true);
					cPS.getChunkAt(cPS, c2.getX(), c2.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c2);
					}
					
					w.unloadChunkRequest(c2.getX(), c2.getZ(), true);	
				}
				if(!c3.isLoaded()) {

					c3.load(true);
					cPS.getChunkAt(cPS, c3.getX(), c3.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c3);
					}
					
					w.unloadChunkRequest(c3.getX(), c3.getZ(), true);
				}
				if(!c4.isLoaded()) {

					c4.load(true);
					cPS.getChunkAt(cPS, c4.getX(), c4.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c4);
					}
					
					w.unloadChunkRequest(c4.getX(), c4.getZ(), true);
				}
								
				if(z >= 0) genZ(x, z - 1, Z, w, act, max, id);
				else genX(x - 1, z, Z, w, act, max, id);
			}	
		}, 1L);
	}
	
	static public void genZ(final int x, final int z, final int Z, final org.bukkit.World w, int actu, final int max, final UUID id) {
		
		final int act = actu + 1;
		
		Player p = Bukkit.getPlayer(id);
		
		if(p != null) {	
			
			if(act == max/10 || act == max/10*2 || act == max/10*3 || act == max/10*4 || act == max/10*5
					|| act == max/10*6|| act == max/10*7 || act == max/10*8 || act == max/10*9 || act == max/10*10) {
				
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Génération des chunks du monde " + ChatColor.DARK_PURPLE 
						+ w.getName() + ChatColor.LIGHT_PURPLE + " à " + ChatColor.DARK_PURPLE + ((int) act * 100 / max + 1) + ChatColor.LIGHT_PURPLE + "%");
			}
			
			if(act == max) {
				
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Génération des chunks du monde " + ChatColor.DARK_PURPLE 
						+ w.getName() + ChatColor.LIGHT_PURPLE + " finit.");
			}
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {

			@Override
			public void run() {
				
				Location l = w.getSpawnLocation();
				
				final Chunk c1 = w.getChunkAt(x + l.getChunk().getX(), z + l.getChunk().getZ());
				final Chunk c2 = w.getChunkAt(x + l.getChunk().getX(), -z + l.getChunk().getZ());
				final Chunk c3 = w.getChunkAt(-x + l.getChunk().getX(), z + l.getChunk().getZ());
				final Chunk c4 = w.getChunkAt(-x + l.getChunk().getX(), -z + l.getChunk().getZ());
					
				ChunkProviderServer cPS = ((CraftWorld)w).getHandle().chunkProviderServer;
				
				if(!c1.isLoaded()) {

					c1.load(true);
					cPS.getChunkAt(cPS, c1.getX(), c1.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c1);
					}
					
					w.unloadChunkRequest(c1.getX(), c1.getZ(), true);
				}				
				if(!c2.isLoaded()) {

					c2.load(true);
					cPS.getChunkAt(cPS, c2.getX(), c2.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c2);
					}
					
					w.unloadChunkRequest(c2.getX(), c2.getZ(), true);	
				}
				if(!c3.isLoaded()) {

					c3.load(true);
					cPS.getChunkAt(cPS, c3.getX(), c3.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c3);
					}
					
					w.unloadChunkRequest(c3.getX(), c3.getZ(), true);
				}
				if(!c4.isLoaded()) {

					c4.load(true);
					cPS.getChunkAt(cPS, c4.getX(), c4.getZ());
					
					for(BlockPopulator bp : w.getPopulators()) {
						
						bp.populate(w, new Random(w.getSeed()), c4);
					}
					
					w.unloadChunkRequest(c4.getX(), c4.getZ(), true);
				}
								
				if(z >= 0) genZ(x, z - 1, Z, w, act, max, id);
				else genX(x - 1, Z, Z, w, act, max, id);
			}		
		}, 1L);
	}
	
	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();
		
		if(check(p)) {
			
			p.teleport(p.getWorld().getSpawnLocation(), TeleportCause.PLUGIN);
			p.getWorld().playSound(p.getWorld().getSpawnLocation(), Sound.ENDERMAN_TELEPORT, 100, 1);
		}
	}
}
