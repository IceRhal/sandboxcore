package net.IceRhal.SandboxCore;
import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class Connection implements Listener {
  
	static boolean bypassFirst = false;
	static FileConfiguration players = Main.players;

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerLogin(PlayerLoginEvent e) {
		
		if(e.getResult() == Result.KICK_FULL) {
			
			Player p = e.getPlayer();
 
			if(p.hasPermission("slots.bypass") || (bypassFirst &&
                    (!players.contains(p.getUniqueId() + ".nb")/*
                            || p.getFirstPlayed() + 60 * 60 * 1000 >= new Date().getTime()*/))) e.allow();
			else {
				
				try {
					
					Iterator<UUID> it = EnPrison.prison.iterator();
					
					while(it.hasNext()) {
					
						Player p2 = Bukkit.getPlayer(it.next());
					
						if(p2 == null) continue;
					
						if(Prison.prison(p2.getUniqueId())) {
							
							p2.kickPlayer(ChatColor.AQUA + "Tu es kick pour laisser les joueurs non emprisonnés joués");
							e.allow();
						}
					}
				
					e.setKickMessage(Langs.slot);
				
				} catch(ConcurrentModificationException ex) {}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {
		
		final Player p = e.getPlayer();
				
		if(!players.contains(p.getUniqueId() + ".nb")) {
			
			int total = players.getInt("total_join") + 1;

			players.set(p.getUniqueId() + ".nb", Integer.valueOf(total));
			players.set("total_join", Bukkit.getOfflinePlayers().length);

			e.setJoinMessage(Langs.firstJoinMessage.replace("$player", p.getName())
					.replace("$nbjoueur", players.getString(p.getUniqueId() + ".nb")));
			
			players.set(p.getName().toLowerCase(), p.getUniqueId().toString());
		} 
		else e.setJoinMessage(Langs.joinMessage.replace("$player", p.getName()));
		
		players.set(p.getName().toLowerCase(), p.getUniqueId().toString());
		Main.cm.saveConfig("players.yml");
		
		Main.players = Main.cm.getConfig("players.yml");
		players = Main.players;
		Aide.players = Main.players;
		Prison.players = Main.players;
				
		for(Player p2 : Bukkit.getOnlinePlayers()) {
			
			Main.setTabList(p2);
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
			@Override
			public void run() {
				
				String list = "";
				Iterator<? extends Player> it = Arrays.asList(Bukkit.getOnlinePlayers()).iterator();
				
				while(it.hasNext()) {
					
					String p2 = it.next().getName();
					
					if(it.hasNext()) {
						
						list += p2 + ", ";						
					}
					else list += p2;
				}				
				
				p.sendMessage(Langs.motd.replace("$playersonline", 
						Bukkit.getOnlinePlayers().length + "").replace("$maxplayers", Bukkit.getMaxPlayers() + "").
						replace("$list", list).replace("$player", p.getName()));
			}		
		}, 5L);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlayerQuit(PlayerQuitEvent e) {
		
		for(Player p2 : Bukkit.getOnlinePlayers()) {
			
			Main.setTabList(p2);
		}
		
		if(EnPrison.prison.contains(e.getPlayer().getUniqueId())) {
			
			EnPrison.prison.remove(e.getPlayer().getUniqueId());
		}
		e.setQuitMessage(Langs.quitMessage.replace("$player", e.getPlayer().getName()));
	}

	static boolean loadConfig() {
						
		ConfigManager cm = new ConfigManager(Main.getPlugin());
		FileConfiguration connection = cm.getConfig("connection.yml");

		cm.saveDefaultConfig("connection.yml");

		bypassFirst = connection.getBoolean("new_Bypass");
		
		if(players.get("total_join") == null) players.set("total_join", Integer.valueOf(0));

		return true;
	}
}