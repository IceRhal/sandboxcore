package net.IceRhal.SandboxCore;

import me.confuser.banmanager.BanManager;
import net.IceRhal.SandboxCore.Chat.ChatAssembler;
import net.IceRhal.SandboxCore.Chat.CustomChar;
import net.IceRhal.SandboxCore.Chat.CustomString;
import net.IceRhal.SandboxCore.Chat.HoverAction;
import net.IceRhal.SandboxCore.Chat.HoverModifier;
import net.IceRhal.SandboxCore.Chat.Modifier;
import net.IceRhal.SandboxCore.Chat.ModifierType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PseudoColor implements Listener {
	
	static Scoreboard sb;
	
	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {
		
		Player p = e.getPlayer();
	
		if(p.hasPermission("deathscoreboard.view")) p.setScoreboard(sb);;
						
		sb.getObjective("morts").getScore(p.getName()).setScore(p.getStatistic(Statistic.DEATHS));
		
		String color = PermissionsEx.getUser(p).getOption("color");
				
		if(color != null && !color.equalsIgnoreCase("")) {
			
			Team t = sb.getTeam(color);
			if(t == null) {
				
				t = sb.registerNewTeam(color);
				t.setCanSeeFriendlyInvisibles(false);
			}
			t.setPrefix(color.replace("&", ChatColor.COLOR_CHAR + ""));
			t.addPlayer(p);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void ASyncPlayerChatMessage(AsyncPlayerChatEvent e) {
		
		if(e.isCancelled()) return;			
		
		Player p = e.getPlayer();
		
		String pre = PermissionsEx.getUser(p).getPrefix().replace("&", ChatColor.COLOR_CHAR + "");
		String suf = PermissionsEx.getUser(p).getSuffix().replace("&", ChatColor.COLOR_CHAR + "");
		String color = PermissionsEx.getUser(p).getOption("color").replace("&", ChatColor.COLOR_CHAR + "");
		
		if(pre == null) pre = "";
		else pre += " ";
		if(suf == null) suf = "";
		else suf += " ";
		if(color == null) color = ChatColor.RESET + "";
		
		int nb = Main.players.getInt(p.getUniqueId() + ".nb");
		
		String hoverText;
		
		if(nb == 1) hoverText = ChatColor.AQUA + Main.players.getString(p.getUniqueId() + ".nb") + " er joueur";
		else hoverText = ChatColor.AQUA + Main.players.getString(p.getUniqueId() + ".nb") + " ème joueur";
							
		for(String m : e.getMessage().split(" ")) {
			
			Player p2 = Bukkit.getPlayerExact(m);
			
			if(p2 == null) continue;
			
			if(!e.getRecipients().contains(p2)) continue;
			
			e.getRecipients().remove(p2);
			
			CustomString s1Modo;
			
			if(Bukkit.getPluginManager().isPluginEnabled("BanManager")) s1Modo = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/prison " + p.getName() + " 15 Non respect des règles du serveur"), "[" + ChatColor.DARK_RED + "X" + ChatColor.RESET + "] ")
				.addModifier(new HoverModifier(HoverAction.SHOW_TEXT, ChatColor.AQUA + "" + BanManager.getPlugin().getPlayerWarnings(p.getName()).size() + " emprisonnement"));
			else s1Modo = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/prison " + p.getName() + " 15 Non respect des règles du serveur"), "[" + ChatColor.DARK_RED + "X" + ChatColor.RESET + "] ");
			
			CustomString s2 = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/msg " + p.getName() + " "), pre + color + p.getName()).addModifier(new Modifier(ModifierType.INSERTION, p.getName()))
						.addModifier(new HoverModifier(HoverAction.SHOW_TEXT, hoverText));
			CustomString s3 = new CustomString(" : " + e.getMessage().replace(m, ChatColor.RED + m + ChatColor.RESET));
								
			if(p2.hasPermission("sandboxcore.chat.prison")) {
				
				new ChatAssembler(new CustomChar(s1Modo, s2).concat(s3)).send(p2);
			}
			else new ChatAssembler(new CustomChar(s2, s3)).send(p2);
			
			p2.playSound(p2.getLocation(), Sound.ORB_PICKUP, 1, 1);
			continue;			
		}

		CustomString s1Modo;
		
		if(Bukkit.getPluginManager().isPluginEnabled("BanManager")) s1Modo = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/prison " + p.getName() + " 15 Non respect des règles du serveur"), "[" + ChatColor.DARK_RED + "X" + ChatColor.RESET + "] ")
			.addModifier(new HoverModifier(HoverAction.SHOW_TEXT, ChatColor.AQUA + "" + BanManager.getPlugin().getPlayerWarnings(p.getName()).size() + " emprisonnement"));
		else s1Modo = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/prison " + p.getName() + " 15 Non respect des règles du serveur"), "[" + ChatColor.DARK_RED + "X" + ChatColor.RESET + "] ");

		CustomString s2 = new CustomString(new Modifier(ModifierType.CLICK_EVENT, "action':'suggest_command','value':'/msg " + p.getName() + " "), pre + color + p.getName()).addModifier(new Modifier(ModifierType.INSERTION, p.getName()))
				.addModifier(new HoverModifier(HoverAction.SHOW_TEXT, hoverText));;
		CustomString s3 = new CustomString(" : " + e.getMessage());
				
		ChatAssembler modo = new ChatAssembler(new CustomChar(s1Modo, s2).concat(s3));
		ChatAssembler player = new ChatAssembler(new CustomChar(s2, s3));
		
		for(Player p2 : e.getRecipients()) {
			
			if(p2.hasPermission("sandboxcore.chat.prison")) {
				
				modo.send(p2);
			}			
			else player.send(p2);
		}
		
		e.getRecipients().clear();
		Bukkit.getConsoleSender().sendMessage(s2.getString().replace("[", "(").replace("]", ")") + s3.getString());
	}
	
	@EventHandler
	public void PlayerQuit(PlayerQuitEvent e) {
		
		e.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
	}
	
	public static boolean load() {		
		
		sb = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective o1 = sb.registerNewObjective("morts", "deathCount");
		o1.setDisplayName("morts");
		o1.setDisplaySlot(DisplaySlot.BELOW_NAME);
		return true;
	}

}
