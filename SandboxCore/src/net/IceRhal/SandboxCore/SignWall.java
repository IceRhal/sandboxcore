package net.IceRhal.SandboxCore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class SignWall extends BukkitRunnable implements Listener {

	static ConfigManager cm;
	static FileConfiguration sign;
	static IntegerFlag signWall = new IntegerFlag("sign-wall");
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void BlockPlace(BlockPlaceEvent e) {
		
		Player p = e.getPlayer();
		
		Iterator<ProtectedRegion> it = WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(e.getBlock().getLocation()).iterator();
			
		ProtectedRegion r = null;
			
		while(it.hasNext()) {
			
			ProtectedRegion reg = it.next();
				
			if(reg.getFlags().containsKey(signWall) && reg.getFlag(signWall) > 0 
						&& (r == null || reg.getPriority() > r.getPriority())) r = reg;
		}
		
		if(r == null) return;
		
		if(p.hasPermission("sandoxcore.signwall.bypass")) return;
		
		if(e.getBlock().getType() != Material.WALL_SIGN) {
			
			p.sendMessage(ChatColor.RED + "Tu peux poser que des pancartes dans cette zone.");
			e.setCancelled(true);
			return;
		}
			
		String path = p.getWorld().getName() + "." + r.getId() + "." + p.getUniqueId();
			
		if(!sign.contains(path)) {
				
			Date d = new Date();
			int var = 0;
			
			if(d.getMinutes() >= 30) var++;
			
			d.setHours(d.getHours() + r.getFlag(signWall) + var);
			sign.set(path + ".expire", d.getTime());
			
			path += ".loc.";
			
			Location l = e.getBlock().getLocation();
			
			sign.set(path + "x", l.getBlockX());
			sign.set(path + "y", l.getBlockY());
			sign.set(path + "z", l.getBlockZ());
			cm.saveConfig("signs.yml");
			
			DateFormat format = new SimpleDateFormat("dd/MM/yy 'à' HH'h'");
			
			p.sendMessage(ChatColor.GREEN + "Ton message va s'autodétruire le " + ChatColor.DARK_GREEN + format.format(d));
			
			return;
		}
		else {
			
			e.setCancelled(true);
			p.sendMessage(ChatColor.RED + "Tu as déjà un message de placé.");
			return;
		}
	}
	
	@EventHandler
	public void BlockBreak(BlockBreakEvent e) {
		
		Player p = e.getPlayer();
		
		Iterator<ProtectedRegion> it = WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(e.getBlock().getLocation()).iterator();
			
		ProtectedRegion r = null;
			
		while(it.hasNext()) {
			
			ProtectedRegion reg = it.next();
				
			if(reg.getFlags().containsKey(signWall) && reg.getFlag(signWall) > 0 
						&& (r == null || reg.getPriority() > r.getPriority())) r = reg;
		}
		
		if(r == null) return;
		
		if(p.hasPermission("sandoxcore.signwall.bypass")) return;
		
		if(e.getBlock().getType() != Material.WALL_SIGN) {
			
			p.sendMessage(ChatColor.RED + "Tu peux casser que des pancartes dans cette zone.");
			e.setCancelled(true);
			return;
		}
		
		String path = p.getWorld().getName() + "." + r.getId() + "." + p.getUniqueId();
		
		if(!sign.contains(path)) {
		
			e.setCancelled(true);
			p.sendMessage(ChatColor.RED + "Tu peux casser seulement ton message.");
			return;
		}
		
		String path2 = path + ".loc.";
		
		Location l1 = e.getBlock().getLocation();
		Location l2 = new Location(l1.getWorld(), sign.getInt(path2 + "x"), sign.getInt(path2 + "y"), sign.getInt(path2 + "z"));
		
		if(!(l1.getWorld() == l2.getWorld() && l1.getBlockX() == l2.getBlockX() && l1.getBlockY() == l2.getBlockY() && l1.getBlockZ() == l2.getBlockZ())) {
			
			e.setCancelled(true);
			p.sendMessage(ChatColor.RED + "Tu peux casser seulement ton message.");
			return;
		}
		
		sign.set(path, null);
		cm.saveConfig("sign.yml");		
	}
	
	@SuppressWarnings("deprecation")
	public static void load() {
		
		cm = new ConfigManager(Main.plugin);
		
		sign = cm.getConfig("signs.yml");
		
		sign.options().copyDefaults(true);
		
		Main.addFlags(signWall);
		
		Date now = new Date();
		long delay = (now.getMinutes() * 60 + now.getSeconds()) * 20;
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new SignWall(), delay, 24 * 60 * 20L);
	}

	@Override
	public void run() {
		
		for(String keys : sign.getKeys(true)) {
			
			String[] k = keys.split("[.]");
			
			if(k.length == 3) {
								
				World w = Bukkit.getWorld(k[0]);
				
				if(w == null) {
					
					sign.set(k[0], null);
					cm.saveConfig("signs.yml");
					continue;
				}
				
				ProtectedRegion rm = WGBukkit.getRegionManager(w).getRegion(k[1]);
				
				if(rm == null) {

					sign.set(k[0] + "." + k[1], null);
					cm.saveConfig("signs.yml");
					continue;
				}
				
				OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(k[2]));
				
				if(!p.hasPlayedBefore()) {					

					sign.set(k[0] + "." + k[1], null);
					cm.saveConfig("signs.yml");
					continue;
				}
				
				Block b = (new Location(w, sign.getInt(keys + ".loc.x"), sign.getInt(keys + ".loc.y"), sign.getInt(keys + ".loc.z"))).getBlock();
			
				if(b.getType() != Material.WALL_SIGN) {
								
					sign.set(k[0] + "." + k[1], null);
					cm.saveConfig("signs.yml");
					continue;
				}
								
				if((new Date()).getTime() >= sign.getLong(keys + ".expire")) {
					
					Location l = b.getLocation();
					
					b.setType(Material.AIR);
					
					sign.set(keys, null);
					cm.saveConfig("signs.yml");
					
					if(!p.isOnline()) continue;
					
					Bukkit.getPlayer(p.getUniqueId()).sendMessage(ChatColor.AQUA + "Ton message dans le monde " 
							+ ChatColor.DARK_AQUA + w.getName() + ChatColor.AQUA + " aux coordonnés : x = " + ChatColor.DARK_AQUA 
							+ l.getBlockX() + ChatColor.AQUA + ", y = " + ChatColor.DARK_AQUA + l.getBlockY() + ChatColor.AQUA 
							+ ", z = " + ChatColor.DARK_AQUA + l.getBlockZ() + ChatColor.AQUA + ", viens de s'autodétruire.");
					
					continue;
				}
			}
			else continue;
		}
	}
}
