package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.confuser.banmanager.BanManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Prison implements CommandExecutor, Listener, TabCompleter {
	
	static FileConfiguration config;
	static FileConfiguration players = Main.players;
	static ConfigManager cm;
	static ConfigManager cmP = Main.cm;
	int task;

	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {
  
		Player p = e.getPlayer();
				
		if((players.contains(p.getUniqueId() + ".jail")) && (players.getLong(p.getUniqueId() + ".jail") > 0L)) EnPrison.put(p); 
	}

	@EventHandler
	public void AsyncPlayerChat(AsyncPlayerChatEvent e)	{
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) {
			
			Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "(En Prison) " + e.getPlayer().getName() + " : " + e.getMessage());		
			e.getPlayer().sendMessage(ChatColor.DARK_RED + "Tu es en prison tu ne peux pas faire ça.");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void PlayerCommandPreprocess(PlayerCommandPreprocessEvent e)	{
		
		UUID id = e.getPlayer().getUniqueId();
		String cmd = e.getMessage().substring(1).split(" ")[0];
		
		if(cmd.equalsIgnoreCase("aide") || cmd.equalsIgnoreCase("msg") || cmd.equalsIgnoreCase("r") || cmd.equalsIgnoreCase("tell")) return;
		
		if(prison(id)) {
			
			e.getPlayer().sendMessage(ChatColor.DARK_RED + "Tu es en prison tu ne peux pas faire ça.");
			e.setCancelled(true);
		}
	}

	@EventHandler
 	public void PlayerDropItem(PlayerDropItemEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void PlayerPickupItem(PlayerPickupItemEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void PlayerInteract(PlayerInteractEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void PlayerInteractEntity(PlayerInteractEntityEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void PlayerPortal(PlayerPortalEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void PlayerItemConsume(PlayerItemConsumeEvent e) {
		
		UUID id = e.getPlayer().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent e) {
		
		UUID id = e.getEntity().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}
	
	@EventHandler
	public void EntityDamageByEntity(EntityDamageByEntityEvent e) {
		
		UUID id = e.getDamager().getUniqueId();

		if(prison(id)) e.setCancelled(true);
	}

	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args)	{
		
		if(label.equalsIgnoreCase("prison")) {
			
			boolean p = false;

			if((s instanceof Player)) p = true;

			if((s.hasPermission("prison.modo")) || (s.hasPermission("prison.admin"))) {
				
				if(args.length >= 1) {
					
					if((args[0].equalsIgnoreCase("set")) && (s.hasPermission("prison.admin")) && (p)) {
						
						Location l = ((Player)s).getLocation();

						config.set("x", Double.valueOf(l.getX()));
						config.set("y", Double.valueOf(l.getY()));
						config.set("z", Double.valueOf(l.getZ()));
						config.set("yaw", Float.valueOf(l.getYaw()));
						config.set("pitch", Float.valueOf(l.getPitch()));
						config.set("world", l.getWorld().getName());

						cm.saveConfig("prison.yml");

						s.sendMessage(ChatColor.GREEN + "Position de tp des joueurs emprisonnés définie.");
						return true;
					}
					else if((args[0]).equalsIgnoreCase("get") && s.hasPermission("prison.modo")) {
						
						if(args.length <= 1) {
							
							s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/prison get [pseudo]");
							return true;
						}
						
						OfflinePlayer p2 = Main.getPlayer(args[1]);

						if(p2 == null)  {
							
							s.sendMessage(ChatColor.DARK_RED + args[1] + ChatColor.RED + " n'est pas en prison.");
							return true;
						}		
						
						if(!prison(p2.getUniqueId()))  {
							
							s.sendMessage(ChatColor.DARK_RED + p2.getName() + ChatColor.RED + " n'est pas en prison.");
							return true;
						}
						
						int time = players.getInt(p2.getUniqueId() + ".jail") / 60000;
						
						s.sendMessage(ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN + " est en prison pendant encore " 
						+ ChatColor.DARK_GREEN + time + ChatColor.GREEN + " minute(s).");
						
						return true;
					}

					OfflinePlayer p2 = null;

					if(Bukkit.getPlayer(args[0]) != null) p2 = Bukkit.getPlayer(args[0]);
					else if(Main.getPlayer(args[0]) != null) p2 = Main.getPlayer(args[0]);

					if(p2 != null && (p2.hasPlayedBefore() || p2.isOnline())) {
						
						PermissionUser u = PermissionsEx.getUser(p2.getName());
						
						if((u.has("prison.modo")) || (u.has("prison.admin")))	{
							
							if(p) s.sendMessage(ChatColor.DARK_RED + p2.getName() + ChatColor.RED + " ne peux pas être emprisonné(e)"); else
								s.sendMessage(p2.getName() + " ne peux pas etre emprisonne(e)");
							return true;
						}
						
						int time = config.getInt("default_time");
						UUID i = p2.getUniqueId();
						String raison = Langs.defaultRaison;

						if(args.length >= 2) {
							
							if(Integer(args[1])) time = Integer.parseInt(args[1]);

							if(((args.length == 2) && (!Integer(args[1]))) || (args.length > 2)) {
								
								raison = "";

								for(String str : args) {
									
									if(args[0] != str || ((!Integer(args[1]) && args[1] != str))) {
										
										if(!Integer(args[1]) && str == args[1]) raison = args[1];
										else if(Integer(args[1]) && str == args[2]) raison = args[2];
										else raison = raison + " " + str;
									}
								}
							}
						}						

						players.set(i + ".raison", raison.replace(ChatColor.COLOR_CHAR + "", "&"));
						
						if(prison(i)) players.set(i + ".jail", players.getInt(i + ".jail") + Integer.valueOf(60000 * time));
						else players.set(i + ".jail", Integer.valueOf(60000 * time));

						cmP.saveConfig("players.yml");

						if(p2.isOnline()) EnPrison.put(Bukkit.getPlayer(p2.getUniqueId()));
													
						if(p) s.sendMessage(ChatColor.GREEN + "Tu as emprisonné le joueur " + ChatColor.DARK_GREEN + p2.getName() 
									+ ChatColor.GREEN + " avec succés pour " + ChatColor.DARK_GREEN + time + ChatColor.GREEN + " minute(s) !");
						else s.sendMessage("Tu as emprisonne le joueur " + p2.getName() + " avec succes pour " 
									+ time + " minute(s) !");
						
						Bukkit.broadcastMessage(Langs.jailedPublicMessage.replace("$player", p2.getName()).replace("$time", time + "")
								.replace("$raison", raison));
						
						if(Bukkit.getPluginManager().getPlugin("BanManager") != null) BanManager.getPlugin().addPlayerWarning(p2.getName(), s.getName(), raison.replaceAll(ChatColor.COLOR_CHAR + ".", ""));
					}
						else if(p) s.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_RED + args[0] 
									+ ChatColor.RED + " est introuvable.");
						else s.sendMessage("Le joueur " + args[0] + " est introuvable.");
				}
				else if(p) s.sendMessage(ChatColor.RED + "Tu dois indiquer le pseudo d'un joueur. (" + ChatColor.DARK_RED + "/prison [pseudo] [durée] [raison]" + ChatColor.RED + ").");
				else s.sendMessage("Tu dois indiquer le pseudo d'un joueur. (/prison [pseudo] [durée] [raison])");
				
			}
			else if(p) s.sendMessage(ChatColor.RED + "Tu n'as pas la permission (" + 
						ChatColor.DARK_RED + "prison.modo" + ChatColor.RED + ") pour exécuter cette commande.");
			else s.sendMessage("Tu n'as pas la permission (prison.modo) pour exécuter cette commande.");
			
			return true;
		}
		if(label.equalsIgnoreCase("unprison")) {
			
			if((!s.hasPermission("prison.modo")) && (!s.hasPermission("prison.admin"))) return true;

			if(args.length == 0) {
				
				s.sendMessage(ChatColor.RED + "Erreur dans la commmande.");
				return true;
			}
			
			boolean p = s instanceof Player;

			OfflinePlayer pl = null;

			if(Bukkit.getPlayer(args[0]) != null) pl = Bukkit.getPlayer(args[0]).getPlayer();
			else if(Main.getPlayer(args[0]) != null) pl = Main.getPlayer(args[0]);

			if(pl != null) {
				
				if(!prison(pl.getUniqueId())) {
					
					if(p) s.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_GREEN + pl.getName() + ChatColor.RED 
						+ " n'est emprisonné.");
					else s.sendMessage("Le joueur " + pl.getName() + " n'est pas emprisonné.");
					
					return true;
				}
				if(pl.isOnline()) players.set(pl.getUniqueId() + ".jail", 0);
				else players.set(pl.getUniqueId() + ".jail", 20L);
				
				cmP.saveConfig("players.yml");

				if(p) s.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + pl.getName() + ChatColor.GREEN 
						+ " vas bientôt sortir de prison.");
				else s.sendMessage("Le joueur " + pl.getName() + " vas bientot sortir de prison.");
			}
			else if(p) s.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_RED + args[0] + ChatColor.RED + " est introuvable.");
			else s.sendMessage("Le joueur " + args[0] + " est introuvable.");
			
			return true;
		}
		s.sendMessage("Commande inconnue.");

		return false;
	}

	boolean Integer(String s) {
		
		try	{
			
			int n = Integer.parseInt(s);

			return n > 0;
		}
		catch (NumberFormatException e)	{
			
			return false;
		}
	}

	static boolean prison(UUID id)	{
		
		return (players.contains(id + ".jail")) && (players.getLong(id + ".jail") > 0L);
	}

	static boolean loadConfig() {
		
		cm = new ConfigManager(Main.getPlugin());

		cm.saveDefaultConfig("prison.yml");

		config = cm.getConfig("prison.yml");

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd,
			String label, String[] args) {
		
		ArrayList<String> s = new ArrayList<String>();
		
		if(label.equalsIgnoreCase("prison")) {
			
			if(args.length == 1) {
				
				for(OfflinePlayer p2 : Bukkit.getOfflinePlayers()) {
					
					if(!s.contains(p2.getName()) && p2.getName().toLowerCase().startsWith(args[0].toLowerCase())) s.add(p2.getName());
					
					if(s.size() >= 10) break;
				}
			}
		}
		else if(label.equalsIgnoreCase("unprison")) {
			
			if(args.length == 1) {
				
				for(OfflinePlayer p2 : Bukkit.getOfflinePlayers()) {
					
					if(!s.contains(p2.getName()) && p2.getName().toLowerCase().startsWith(args[0].toLowerCase()) && prison(p2.getUniqueId())) s.add(p2.getName());
					
					if(s.size() >= 10) break;
				}
			}
		}
		
		return s;
	}
}