package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Annonce implements CommandExecutor {
	
	static ConfigManager cm;
	static FileConfiguration config;
	static ArrayList<String> aList = new ArrayList<String>();
	static Boolean a = false;

	static boolean load() {
		
		cm = new ConfigManager(Main.getPlugin());

		cm.saveDefaultConfig("annonce.yml");

		config = cm.getConfig("annonce.yml");

		final List<String> ls = config.getStringList("annonces");

		for(int x = 0; x < ls.size(); x++)
		{
			final int y = x;

			Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable()
			{
				public void run()
				{
					Bukkit.broadcastMessage(config.getString("prefix").replace("&", ChatColor.COLOR_CHAR + "") + 
							ChatColor.RESET + ls.get(y).replace("&", ChatColor.COLOR_CHAR + ""));
				}
			}
			, x * config.getLong("delay") * 20L * 60L, ls.size() * config.getLong("delay") * 20L * 60L);
		}

		return true;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(label.equalsIgnoreCase("annonce")) {
			
			if(sender.hasPermission("sandboxcore.annonce")) {
				
				/*if(a) {
					
					if((sender instanceof Player)) sender.sendMessage(ChatColor.RED + "Il y a déjà une annonce en cours."); 
					else sender.sendMessage("Il y a deja une annonce en cours.");
					
					return true;
				}*/
				
				if(args.length == 0) {
					
					if((sender instanceof Player)) sender.sendMessage(ChatColor.RED + "Il faut préciser une annonce."); 
					else sender.sendMessage("Il faut preciser une annonce.");
					
					return true;
				}
				
				String annonce = "";

				for(String s : args) annonce = annonce + s + " ";

				aList.add(annonce.replace("&", ChatColor.COLOR_CHAR + ""));
								
				if(a) {

					if((sender instanceof Player)) sender.sendMessage(ChatColor.GREEN + "L'annonce sera diffusé après celle(s) en cours."); 
					else sender.sendMessage("L'annonce sera diffusé après celle(s) en cours.");
				}
				else {
					
					annonce();
					
					if((sender instanceof Player)) sender.sendMessage(ChatColor.GREEN + "Annonce envoyée !"); 
					else sender.sendMessage("Annonce envoyee !");
				}
			}
			else if((sender instanceof Player)) sender.sendMessage(ChatColor.RED + "Tu n'as pas la permission (" + 
					ChatColor.DARK_RED + "annonce.admin" + ChatColor.RED + ") pour éxécuter cette commande.");
			
			else sender.sendMessage("Tu n'as pas la permission (annonce.admin) pour éxécuter cette commande.");

			return true;
		}
		sender.sendMessage("Commande inconnue.");

		return false;
	}
	
	@SuppressWarnings("deprecation")
	public static void annonce() {
		
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + 
				aList.get(0) + "\"}");

		final PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);

		final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable()
		{
			
			public void run()
			{
				for (Player p : Bukkit.getOnlinePlayers())
				{
					((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoc);
				}
			}
		}
		, 0L, 10L);

		for(Player p : Bukkit.getOnlinePlayers()) p.playSound(p.getLocation(), Sound.ORB_PICKUP, 100.0F, 0.5F);

		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				
				a = false;
				Bukkit.getScheduler().cancelTask(task);		
				aList.remove(0);
				if(!aList.isEmpty()) annonce();
			}
		}, config.getInt("delay_actionbar") * 20L);
		
		a = true;
	}
}