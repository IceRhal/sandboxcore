package net.IceRhal.SandboxCore;

import java.util.ArrayList;

import me.desht.dhutils.ExperienceManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Xp implements Listener, CommandExecutor {
	
	static ItemStack is;
    static ItemStack is2;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(sender instanceof Player) {
			
			if(label.equalsIgnoreCase("xp")) {
				
				Player p = (Player) sender;
				
				if(Main.hasPerm(p, "sandboxcore.xp")) {
					
					ExperienceManager em = new ExperienceManager(p);
					
					/*int exp = em.getCurrentExp() / 10;
					
					if(exp * 10 > em.getCurrentExp()) exp--;
					
					int xpUse = 0;
					
					for(int x = exp; x > 0; x--) {
						
						if(p.getInventory().addItem(is).isEmpty()) {
							
							xpUse += 10;
						}
						else break;
					}*/

                    if(em.getCurrentExp() >= 825) {

                        if(p.getInventory().addItem(is2).isEmpty()) {

                            em.changeExp(-825);

                            p.sendMessage(ChatColor.GREEN + "Tu viens de convertir " + ChatColor.DARK_GREEN + "30"
                                    + ChatColor.GREEN + " niveaux dans une fiole d'expérience.");
                        }
                        else {

                            p.sendMessage(ChatColor.RED + "Pour convertir ton expérience tu as besoin d'avoir une place dans ton inventaire.");
                        }
                    }
                    else p.sendMessage(ChatColor.RED + "Pour convertir ton expérience tu doit être au moins niveau "
                                    + ChatColor.DARK_RED + "30" + ChatColor.RED + ".");

				}
				
				return true;
			}
			else return false; 
		}
		else sender.sendMessage("Cette commande doit etre faites par un joueur.");
		return false;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void ProjectileLaunch(ProjectileLaunchEvent e) {
		
		Projectile ent = e.getEntity();
		
		if(ent instanceof ThrownExpBottle) {
			
			if(ent.getShooter() instanceof Player) {

                if(((Player)ent.getShooter()).getItemInHand().isSimilar(is)) {

                    ent.setCustomNameVisible(false);
                    ent.setCustomName(ChatColor.AQUA + "Sandbox Expérience 10");
                }
                else if(((Player) ent.getShooter()).getItemInHand().isSimilar(is2)) {

                    ent.setCustomNameVisible(false);
                    ent.setCustomName(ChatColor.AQUA + "Sandbox Expérience 1395");
                }
			}
		}
	}
	
	@EventHandler
	public void BlockDispense(BlockDispenseEvent e) {
		
		if(e.getItem().isSimilar(is) || e.getItem().isSimilar(is2)) {
			
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void ExpBottle(ExpBottleEvent e) {
		
		if(e.getEntity().getCustomName().equalsIgnoreCase(ChatColor.AQUA + "Sandbox Expérience 10")) {
			
			new ExperienceManager((Player)e.getEntity().getShooter()).changeExp(10);
			e.setExperience(0);
		}
        else if(e.getEntity().getCustomName().equalsIgnoreCase(ChatColor.AQUA + "Sandbox Expérience 1395")) {

            new ExperienceManager((Player)e.getEntity().getShooter()).changeExp(825);
            e.setExperience(0);
        }
	}
	
	
	static void load() {
		
		is = new ItemStack(Material.EXP_BOTTLE, 1);
		
		ItemMeta im = is.getItemMeta();
		
		im.setDisplayName(ChatColor.AQUA + "Expérience Sandbox");
		
		ArrayList<String> ls = new ArrayList<String>();
		
		ls.add(ChatColor.GOLD + "Cette bouteille te permet d'avoir 10 points d'expérience !");
		
		im.setLore(ls);
		
		is.setItemMeta(im);

        is2 = new ItemStack(Material.EXP_BOTTLE, 1);

        im = is2.getItemMeta();

        im.setDisplayName(ChatColor.AQUA + "Expérience Sandbox");

        ls = new ArrayList<String>();

        ls.add(ChatColor.GOLD + "Cette bouteille te pemet d'avoir 1395 points d'expérience (30 lvl) !");

        im.setLore(ls);

        is2.setItemMeta(im);
	}

}
