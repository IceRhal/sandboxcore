package net.IceRhal.SandboxCore;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spawner implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(label.equalsIgnoreCase("spawner") && sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(!Main.hasPerm(p, "sandboxcore.spawner")) return true;
				
			if(args.length == 0) {
					
				p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + " /spawner [mob]");
				return true;
			}
			
			Location l = p.getLocation();
			
			l.setY(l.getBlockY() - 1);
			
			Block b = l.getBlock();
			
			b.setType(Material.STONE);
			b.setType(Material.MOB_SPAWNER);
			CreatureSpawner spawner = (CreatureSpawner) b.getState();
			
			spawner.setCreatureTypeByName(args[0]);
			
			p.sendMessage(ChatColor.GREEN + "Tu viens de créer un spawner à " + ChatColor.DARK_GREEN + spawner.getCreatureTypeName() + ChatColor.GREEN + " sous tes pieds !");			
			return true;
		}
		else sender.sendMessage("Commande incorrecte.");		
		return false;
	}

}
