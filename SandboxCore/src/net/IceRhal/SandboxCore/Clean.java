package net.IceRhal.SandboxCore;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class Clean implements CommandExecutor, Listener {
	
	static HashMap<Player, ItemStack[][]> clean = new HashMap<Player, ItemStack[][]>();
	static ItemStack[] blocs = new ItemStack[9];

	@EventHandler
	public void PlayerPickupItem(PlayerPickupItemEvent e) {
		
		if(clean.containsKey(e.getPlayer())) e.setCancelled(true);
	}
	
	@EventHandler
	public void PlayerDropItem(PlayerDropItemEvent e) {
		
		if(clean.containsKey(e.getPlayer())) e.setCancelled(true);
	}
	
	@EventHandler
	public void PlayerQuit(PlayerQuitEvent e) {
		
		Player p = e.getPlayer();
		
		if(clean.containsKey(p)) {
			
			p.setGameMode(GameMode.SURVIVAL);
			p.getInventory().clear();
			p.getInventory().setArmorContents(clean.get(p)[0]);
			p.getInventory().setContents(clean.get(p)[1]);
								
			clean.remove(p);
		}
	}
	
	@EventHandler
	public void InventoryOpen(InventoryOpenEvent e) {
		
		if(clean.containsKey(e.getPlayer())) e.setCancelled(true);
	}
	
	@EventHandler
	public void InventoryClick(InventoryClickEvent e) {
		
		if(clean.containsKey(e.getWhoClicked())) e.setCancelled(true);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(label.equalsIgnoreCase("clean")) {
				
				if(!p.hasPermission("clean.use")) {
					
					p.sendMessage(ChatColor.RED + "Tu n'as pas la permission (" + ChatColor.DARK_RED + "clean.use" + ChatColor.RED 
							+ ") pour éxécuter cette commande.");
					return true;
				}
				
				if(clean.containsKey(p)) {

					p.setGameMode(GameMode.SURVIVAL);
					p.getInventory().clear();
					p.getInventory().setArmorContents(clean.get(p)[0]);
					p.getInventory().setContents(clean.get(p)[1]);
										
					clean.remove(p);
					
					p.sendMessage(ChatColor.GREEN + "Mode clean désactivé.");
					Bukkit.broadcast(ChatColor.GOLD + p.getName() + ChatColor.YELLOW + " a désactivé le mode clean.", "sandboxcore.clean.broadcast");
					
					return true;
				}
				else {
					
					p.setGameMode(GameMode.CREATIVE);
					
					ItemStack[][] is = new ItemStack[2][];
					
					is[0] = p.getInventory().getArmorContents();
					is[1] = p.getInventory().getContents();
										
					clean.put(p, is);
					
					p.getInventory().clear();
					p.getInventory().setArmorContents(null);
					
					for(ItemStack i : blocs) {
						
						if(i != null) p.getInventory().addItem(i);
					}
					
					p.sendMessage(ChatColor.GREEN + "Mode clean activé.");
					Bukkit.broadcast(ChatColor.GOLD + p.getName() + ChatColor.YELLOW + " a activé le mode clean.", "sandboxcore.clean.broadcast");
					
					return true;
				}
			}			
		}
		else sender.sendMessage("Tu dois etre un joueur pour executer cette commande.");
		
		return false;
	}
	
	@SuppressWarnings("deprecation")
	static boolean loadConfig() {
		
		ConfigManager cM = new ConfigManager(Main.plugin);
		
		cM.saveDefaultConfig("clean.yml");

		FileConfiguration config = cM.getConfig("clean.yml");
		
		int x = 0;
		for(String str : config.getStringList("stuff")) {			
			
			String[] s = str.split(":");
			
			if(s.length == 1) blocs[x] = new ItemStack(Material.getMaterial(Integer.parseInt(s[0])), 1);
			else if(s.length == 2) blocs[x] = new ItemStack(Material.getMaterial(Integer.parseInt(s[0])), 1, (short) Integer.parseInt(s[1]));
			else blocs[x] = new ItemStack(Material.AIR);
			
			x++;
		}
		
		return true;
	}
}
