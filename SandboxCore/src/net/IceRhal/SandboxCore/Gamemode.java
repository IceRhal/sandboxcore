package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class Gamemode implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		
		if(s instanceof Player) {
			
			Player p = (Player) s;
			
			if(label.equalsIgnoreCase("gamemode") || label.equalsIgnoreCase("gm")) {
				
				if(!Main.hasPerm(p, "sandboxcore.gamemode")) return true;
				
				GameMode g;
				Player p2 = null;
				if(args.length == 0) {
					
					if(p.getGameMode() != GameMode.CREATIVE) g = GameMode.CREATIVE;
					else g = GameMode.SURVIVAL;
				}
				else {
					
					switch(args[0]) {
					
					case "0": g = GameMode.SURVIVAL; break;
					case "1": g = GameMode.CREATIVE; break;
					case "2": g = GameMode.ADVENTURE; break;
					case "3": g = GameMode.SPECTATOR; break;
					
					default : p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/" + label + " [0|1|2|3] [player]");
					return true;
					}
					
					if(args.length > 1) {
						
						p2 = Bukkit.getPlayer(args[1]);
						
						if(p2 == null) {
							
							p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " inconnue.");
							return true;
						}						
					}
				}
				
				String gm = "";
				
				switch(g) {
				
				case SURVIVAL: gm = "survie"; break;
				case CREATIVE: gm = "creatif"; break;
				case ADVENTURE: gm = "aventure"; break;
				case SPECTATOR: gm = "spectateur"; break;
				}
				
				if(p2 != null) {
					
					p2.setGameMode(g);
					p2.sendMessage(ChatColor.GREEN + "Tu viens de passer en mode " + ChatColor.DARK_GREEN + gm);
					p.sendMessage(ChatColor.GREEN + "Tu viens de mettre " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN + " en mode " + ChatColor.DARK_GREEN + gm);
				}
				else {
					
					p.setGameMode(g);
					p.sendMessage(ChatColor.GREEN + "Tu viens de passer en mode " + ChatColor.DARK_GREEN + gm);
				}								
				return true;
			}
			else p.sendMessage(ChatColor.RED + "Commande inconnue.");
		}
		else s.sendMessage("Cette commande doit être faites par un joueur.");
		
		return false;
	}

	

	@SuppressWarnings("deprecation")
	@Override
	public List<String> onTabComplete(CommandSender s, Command cmd, String label, String[] args) {

		ArrayList<String> sList = new ArrayList<String>();
		
		if(args.length == 1) {
			
			sList.add("0");
			sList.add("1");
			sList.add("2");
			sList.add("3");
		}
		else if(args.length == 2) {
			
			for(Player p : Bukkit.getOnlinePlayers()) {
				
				if(p.getName().toLowerCase().startsWith(args[1].toLowerCase())) sList.add(p.getName());
				if(sList.size() >= 10) break;
			}
		}
		
		return sList;
	}
}
