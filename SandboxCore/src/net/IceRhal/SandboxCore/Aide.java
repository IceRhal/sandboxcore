package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class Aide implements CommandExecutor, Listener {

    static ArrayList<String> book = new ArrayList<String>();
    static ArrayList<String> admin_book = new ArrayList<String>();    
    static FileConfiguration players = Main.players;
    static FileConfiguration config;
    static ConfigManager cm;
    static ItemStack is;
    
	public boolean onCommand(CommandSender sender, Command commande, String label,
    		String[] args) {
		
		if(sender instanceof Player) {
			
			Player p = (Player)sender;
			           
			if(p.hasPermission("aide.admin")) {
				
                int size = book.size() + admin_book.size();
                int slot = size - size % 9;
                
                if(size % 9 != 0) slot += 9;
                
                Inventory inv = Bukkit.createInventory(null, slot, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Livres d'Aide");
                
                for(String s : book) {
                	
                	is = new ItemStack(Material.WRITTEN_BOOK);
                    is.setItemMeta(getBook(p, s));
                    inv.addItem(is);
                }

                for(String s : admin_book) {
                	
                	is = new ItemStack(Material.WRITTEN_BOOK);
                    is.setItemMeta(getBook(p, s));
                    inv.addItem(is);
                }
                
                p.openInventory(inv);                
            }				
			else if(p.hasPermission("aide.user")) {
				
				int slot = book.size() - book.size() % 9;
				
                if(book.size() % 9 != 0) slot += 9;
               
                Inventory inv = Bukkit.createInventory(null, slot, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Livres d'Aide");
                
                for(String s : book) {
                	
                	is = new ItemStack(Material.WRITTEN_BOOK);
                    is.setItemMeta(getBook(p, s));
                    inv.addItem(is);
                }

                p.openInventory(inv);                
            } 
			else p.sendMessage(ChatColor.RED + "Tu n'as pas la permission (" + ChatColor.DARK_RED 
					+ "aide.user" + ChatColor.RED + ") pour éxécuter cette commande.");

            return true;
		}		
		else sender.sendMessage("Cette commande doit etre executer par un joueur.");
		
		return false;
    }

    @SuppressWarnings("deprecation")
    @EventHandler
	public void InventoryClick(InventoryClickEvent e) {
       
    	if(e.getView().getTitle().contains(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Livres d'Aide")) {
    		
        		e.setCancelled(true);
        		
            	try {
                    
            		if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta().hasLore()) {
                       
            			List<String> lore = e.getCurrentItem().getItemMeta().getLore();
            			
                        if(lore != null && (lore.contains(ChatColor.GREEN + "Livre d'Aide") 
                        		|| lore.contains(ChatColor.RED + "Livre d'Aide Staff"))) {
                        	
                        	e.getWhoClicked().getInventory().addItem(e.getCurrentItem());
                        }
                    } 
            		
            		else if(e.getCursor() != null && e.getCursor().getItemMeta().hasLore()) {

                        List<String> lore = e.getCursor().getItemMeta().getLore();
                        
                        if(lore != null && (lore.contains(ChatColor.GREEN + "Livre d'Aide") 
                        		|| lore.contains(ChatColor.RED + "Livre d'Aide Staff")))
                        	
                            e.getWhoClicked().getInventory().addItem(e.getCursor());
                    }
                } catch(NullPointerException e1) {}
    	}
    	
        if(e.getView() != e.getWhoClicked().getInventory() 
        		&& !e.getView().getTitle().contains(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Livres d'Aide")) {
        	
        	try {
                
        		if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta().hasLore()) {
                   
        			List<String> lore = e.getCurrentItem().getItemMeta().getLore();
        			
                    if(lore != null && (lore.contains(ChatColor.GREEN + "Livre d'Aide") 
                    		|| lore.contains(ChatColor.RED + "Livre d'Aide Staff")))
                        e.setCurrentItem(new ItemStack(Material.AIR));
                } 
        		
        		else if(e.getCursor() != null && e.getCursor().getItemMeta().hasLore()) {

                    List<String> lore = e.getCursor().getItemMeta().getLore();
                    
                    if(lore != null && (lore.contains(ChatColor.GREEN + "Livre d'Aide") 
                    		|| lore.contains(ChatColor.RED + "Livre d'Aide Staff")))
                        e.setCursor(new ItemStack(Material.AIR));
                }
            } catch(NullPointerException e1) {}
        }
        else return;
    }

    @EventHandler
    public void ItemSpawn(ItemSpawnEvent e) {
    	
        if(e.getEntity().getItemStack().getItemMeta().hasLore()) {
        	
            List<String> lore = e.getEntity().getItemStack().getItemMeta().getLore();
            
            if(lore.contains(ChatColor.GREEN + "Livre d'Aide") || lore.contains(ChatColor.RED + "Livre d'Aide Staff"))
                e.setCancelled(true);
        }
    }

    static boolean loadConfig() {
    	
        cm = new ConfigManager(Main.getPlugin());
        cm.saveDefaultConfig("aide.yml");
        config = cm.getConfig("aide.yml");
        
        admin_book = new ArrayList<String>();
        book = new ArrayList<String>();
        
        for(String key : config.getKeys(true)) {
        	
            String k[] = key.split("[.]");
            
            if(k.length == 1) {
            	
            	if(config.contains(key + ".admin_help") && config.getBoolean(key + ".admin_help")) admin_book.add(key);
                else book.add(key);
            }
        }

        return true;
    }

    BookMeta getBook(Player p, String book) {
    	
        BookMeta bM = (BookMeta) new ItemStack(Material.WRITTEN_BOOK).getItemMeta();
        
        if(config.getString(book + ".title") == null) {
        	
            bM.setDisplayName(book);
            bM.setTitle(book);
        } 
        else {
        	
            bM.setDisplayName(format(config.getString(book + ".title"), p));
            bM.setTitle(format(config.getString(book + ".title"), p));
        }
        
        if(config.getString(book + ".author") == null) bM.setAuthor(ChatColor.MAGIC + "Unkown");
        else bM.setAuthor(format(config.getString(book + ".author"), p));
        
        int xMax = 1;
        
        for(int x = 1; x <= 50; x++) if(config.getString(book + ".pages." + x) != null) xMax = x;

        for(int x = 1; x <= xMax; x++) {
        	
        	if(config.getString(book + ".pages." + x) != null) 
        		bM.addPage(format(config.getString(book + ".pages." + x), p));
            else bM.addPage(" ");
        }

        ArrayList<String> lore = new ArrayList<String>();
        
        if(config.getBoolean(book + ".admin_help")) lore.add(ChatColor.RED + "Livre d'Aide Staff");
        else lore.add(ChatColor.GREEN + "Livre d'Aide");
        
        bM.setLore(lore);
        
        return bM;
    }

    String format(String s, Player p) {
    	
    	s = s.replace("&", ChatColor.COLOR_CHAR + "").replace("$joueur", p.getName())
        		.replace("$uuid", p.getUniqueId() + "")
        		.replace("$nbjoueur", players.getString(p.getUniqueId() + ".nb"))
        		.replace("$nbtotaljoueur", players.getString("total_join")).replace("$$", "\n");
    	
        Location b = p.getBedSpawnLocation();
        
        if(b != null) {
        	
            s = s.replace("$x", b.getX() + "")
            		.replace("$y", b.getY() + "")
            		.replace("$z", b.getZ() + "")
            		.replace("$world", b.getWorld().getName());
        } 
        else {
        	
            String no = ChatColor.RED + "Aucun lit";
            s = s.replace("$x", no).replace("$y", no).replace("$z", no).replace("$world", no);
        }
        
        return s;
    }
}
