package net.IceRhal.SandboxCore;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class Spawn implements CommandExecutor, Listener {

	static ConfigManager cm;
	static FileConfiguration config;
	static HashMap<World, Location> Spawn = new HashMap<World, Location>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(label.equalsIgnoreCase("spawn")) {
				
				if(!p.hasPermission("sandboxcore.spawn")) {
					
					p.sendMessage(ChatColor.AQUA + "Coordonnée du spawn : X " + ChatColor.DARK_AQUA 
							+ p.getWorld().getSpawnLocation().getBlockX() + ChatColor.AQUA + ", Z " 
							+ ChatColor.DARK_AQUA + p.getWorld().getSpawnLocation().getBlockZ());
					return true;
				}
				
				p.teleport(Spawn.get(p.getWorld()), TeleportCause.COMMAND);
				p.getWorld().playSound(Spawn.get(p.getWorld()), Sound.ENDERMAN_TELEPORT, 1, 1);
				p.sendMessage(ChatColor.GREEN + "Tu viens de te tp au spawn de ton monde.");
			}
			else if(label.equalsIgnoreCase("setspawn")) {

				if(!Main.hasPerm(p, "sandboxcore.setspawn")) return true;
				
				String path = p.getWorld().getName() + ".";
				Location loc = p.getLocation();
				
				config.set(path + "x", loc.getX());
				config.set(path + "y", loc.getY());
				config.set(path + "z", loc.getZ());
				config.set(path + "yaw", loc.getYaw());
				config.set(path + "pitch", loc.getPitch());
				cm.saveConfig("spawns.yml");
				
				Spawn.put(p.getWorld(), loc);
				p.getWorld().setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
				p.sendMessage(ChatColor.GREEN + "Tu viens de définir le spawn du monde " + ChatColor.DARK_GREEN + p.getWorld().getName());
			}
			else p.sendMessage(ChatColor.RED + "Commande inconnue.");
			
			return true;
		}
		else sender.sendMessage("Seul un joueur peut faire cette commande.");
		
		return false;
	}
	
	@EventHandler
	public void PlayerRespawn(PlayerRespawnEvent e) {
		
		if(HomeBed.getBed(e.getPlayer()) == null) e.setRespawnLocation(e.getPlayer().getWorld().getSpawnLocation());
		else e.setRespawnLocation(HomeBed.getBed(e.getPlayer()));
		
		Main.setTabList(e.getPlayer());
	}
	
	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {
		
		if(!e.getPlayer().hasPlayedBefore()) {
			
			e.getPlayer().teleport(Spawn.get(e.getPlayer().getWorld()));
		}
	}
	
 	public static boolean loadConfig() {
		
		cm = new ConfigManager(Main.getPlugin());

		cm.saveDefaultConfig("spawns.yml");

		config = cm.getConfig("spawns.yml");
		
		for(World w : Bukkit.getWorlds()) {
			
			String path = w.getName() + ".";
			Location loc;
			
			if(config.contains(path + "x")) {
								
				loc = new Location(w, config.getInt(path + "x"), config.getInt(path + "y"), config.getInt(path + "z"),
						(float) config.getDouble(path + "yaw"), (float) config.getDouble(path + "pitch"));				
			}
			else loc = w.getSpawnLocation();
			
			w.setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
			Spawn.put(w, loc);
		}
		
		return true;
	}

}
