package net.IceRhal.SandboxCore;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import me.confuser.banmanager.BanManager;
import net.IceRhal.SandboxCore.Entity.IceCow;
import net.IceRhal.SandboxCore.Entity.NMSUtils;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.EntityCow;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutPlayerListHeaderFooter;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.Statistic;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import sun.font.TextRecord;

public class Main extends JavaPlugin implements Listener {
	
	static Main plugin;
	static ConfigManager cm;
	static FileConfiguration players;
	static FileConfiguration arret;
	static int res;
	PluginManager pm;

	@SuppressWarnings("deprecation")
	public void onEnable() {
		
		plugin = this;
		pm = Bukkit.getPluginManager();
		cm = new ConfigManager(this);
		
		ConfigManager cmArret = new ConfigManager(this);
		
		cm.saveDefaultConfig("players.yml");
		cmArret.saveDefaultConfig("arret.yml");
		
		players = cm.getConfig("players.yml");
		arret = cmArret.getConfig("arret.yml");	
		
		getCommand("annonce").setExecutor(new Annonce());
		getCommand("aide").setExecutor(new Aide());
		getCommand("prison").setExecutor(new Prison());
		getCommand("unprison").setExecutor(new Prison());
		getCommand("clean").setExecutor(new Clean());
		getCommand("temp").setExecutor(new Temp());
		getCommand("vip").setExecutor(new Temp());
		getCommand("gamemode").setExecutor(new Gamemode());
		getCommand("gm").setExecutor(new Gamemode());
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("setspawn").setExecutor(new Spawn());
		getCommand("warp").setExecutor(new Warp());
		getCommand("warps").setExecutor(new Warp());
		getCommand("setwarp").setExecutor(new Warp());
		getCommand("delwarp").setExecutor(new Warp());
		getCommand("msg").setExecutor(new Message());
		getCommand("mp").setExecutor(new Message());
		getCommand("m").setExecutor(new Message());
		getCommand("tell").setExecutor(new Message());
		getCommand("r").setExecutor(new Message());
		getCommand("setborder").setExecutor(new Border());
		getCommand("genborder").setExecutor(new Border());
		getCommand("xp").setExecutor(new Xp());
		getCommand("staff").setExecutor(new Staff());
		getCommand("s").setExecutor(new Staff());
		getCommand("spawner").setExecutor(new Spawner());
		
		getCommand("prison").setTabCompleter(new Prison());
		getCommand("unprison").setTabCompleter(new Prison());
		getCommand("temp").setTabCompleter(new Temp());
		getCommand("gamemode").setTabCompleter(new Gamemode());
		getCommand("gm").setTabCompleter(new Gamemode());
		getCommand("warp").setTabCompleter(new Warp());
		getCommand("delwarp").setTabCompleter(new Warp());
		
		pm.registerEvents(new Connection(), plugin);
		pm.registerEvents(new Aide(), plugin);
		pm.registerEvents(new Prison(), plugin);
		pm.registerEvents(new Clean(), plugin);
		pm.registerEvents(new Temp(), plugin);
		pm.registerEvents(new PseudoColor(), plugin);
		pm.registerEvents(new Spawn(), plugin);
		pm.registerEvents(new EasterEgg(), plugin);
		pm.registerEvents(new Border(), plugin);
		pm.registerEvents(new NetherRatio(), plugin);
		pm.registerEvents(new Flags(), plugin);
		pm.registerEvents(new HomeBed(), plugin);
		pm.registerEvents(new Wither(), plugin);
		pm.registerEvents(new Tp(), plugin);
		pm.registerEvents(new Rain(), plugin);
		pm.registerEvents(new Xp(), plugin);

		pm.registerEvents(this, plugin);
		
		if(pm.isPluginEnabled("WorldEdit")) pm.registerEvents(this, this);
		if(pm.isPluginEnabled(WGBukkit.getPlugin())) pm.registerEvents(new SignWall(), plugin);

		Connection.loadConfig();
		Langs.loadConfig();
		Aide.loadConfig();
		Prison.loadConfig();
		Clean.loadConfig();
		Spawn.loadConfig();
		Warp.loadConfig();
		NetherRatio.loadConfig();
		Rain.loadConfig();
				
		Annonce.load();
		PseudoColor.load();
		Flags.load();
		HomeBed.load();
		Xp.load();
		
		if(pm.isPluginEnabled(WGBukkit.getPlugin())) SignWall.load();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new AFK(), 60 * 20L, 60 * 20L);	
		
	    NMSUtils nms = new NMSUtils();
        nms.registerEntity("Cow", 92, EntityCow.class, IceCow.class);
        
        try {
        	
            Metrics metrics = new Metrics(this);
            
            metrics.start();
        } catch (IOException e) {
            // Failed to submit the stats :-(
        }       
        
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

			@Override
			public void run() {

				System.out.print("[DEBUG] Update flag");
				Server s = Bukkit.getServer();
				CommandSender se = Bukkit.getConsoleSender();
				
				s.dispatchCommand(se, "aclaim update");
				s.dispatchCommand(se, "rg flag -w world map creeper-grief deny");
				s.dispatchCommand(se, "rg flag -w world map claimable allow");	
				s.dispatchCommand(se, "rg flag -w world_nether nether claimable allow");
			}        	
        	
        }, 5 * 20L);
	}

	public void onDisable() {	  
	  
	}

	public static Main getPlugin() {
		
		return plugin;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender s, Command cmd, String label, String[] args) {		
    
		boolean pl = s instanceof Player;
		if(label.equalsIgnoreCase("rlcore")) {
			
			if(!hasPerm(s, "sandboxecore.reload")) return true;
			
			Connection.loadConfig();
			Langs.loadConfig();
			Aide.loadConfig();
			Prison.loadConfig();
			Clean.loadConfig();
			Spawn.loadConfig();
			Warp.loadConfig();
			
			Annonce.load();
			PseudoColor.load();
			
			ConfigManager cmArret = new ConfigManager(this);
			
			cmArret.saveDefaultConfig("arret.yml");
			arret = cmArret.getConfig("arret.yml");
      
			if(pl) s.sendMessage(ChatColor.GREEN + "Plugin " + ChatColor.DARK_GREEN + "SandboxCore" + ChatColor.GREEN + " reload avec succés.");
			else s.sendMessage("Plugin SandboxCore reload avec succes");
		  
			return true;
			
		}
		else if(label.equalsIgnoreCase("arret")) {
			
			if(!hasPerm(s, "sandboxcore.arret")) return true;
			
			if(args.length <= 0) {
				
				if(pl) s.sendMessage(ChatColor.RED + "Tu dois préciser une raison pour arreter le serveur (" 
						+ ChatColor.DARK_RED + "/arret [raison]" + ChatColor.RED + ")."); 
				else s.sendMessage("Tu dois preciser une raison pour arreter le serveur (/arret [raison]).");
				
				return true;
			}
			else {
				
				int delay = arret.getInt("delay");
				res = delay;
				String raison = args[0];
				
				for(String str : args) {
					
					if(!str.equals(args[0])) raison = raison + " " + str;
				}
				
				final String raison2 = raison.replace("&", ChatColor.COLOR_CHAR + "");

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					@Override
					public void run() {

                        for(Player p : Bukkit.getOnlinePlayers()) {

							p.kickPlayer(raison2);
						}

						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							@Override
							public void run() {
								Bukkit.shutdown();

							}
						},5L);
					}
				}, delay * 20L - 5L);
			}
			
			return true;
		}
		else if(label.equalsIgnoreCase("p")) {
			
			if(!hasPerm(s, "sandboxcore.p")) return true;
			
			if(!pl) s.sendMessage("Cette commande doit etre faites par un joueur");
			
			String name = ((Player) s).getName();
			
			if(args.length > 0) name = Bukkit.getOfflinePlayer(args[0]).getName();
			
			ItemStack is = new ItemStack(Material.SKULL_ITEM, 1 , (short) 3);
			
			SkullMeta im = (SkullMeta) is.getItemMeta();
						
			im.setOwner(name);
			is.setItemMeta(im);
			
			((Player) s).getInventory().addItem(is);
			s.sendMessage(ChatColor.GREEN + "Tu viens de recevoir la tête de " + ChatColor.DARK_GREEN + name);
			
			return true;
		}
		else if(label.equalsIgnoreCase("info") || label.equalsIgnoreCase("sbinfo")) {
			
			if(!hasPerm(s, "sandboxcore.info")) return true;
			
			OfflinePlayer p2 = Bukkit.getOfflinePlayer(s.getName());
			
			if(args.length > 0) p2 = Bukkit.getOfflinePlayer(args[0]);
			
			ArrayList<String> l = new ArrayList<String>();
			
			l.add(ChatColor.DARK_GREEN + "#### " + ChatColor.AQUA + "Info sur " + ChatColor.DARK_AQUA + p2.getName() + ChatColor.DARK_GREEN + " ####");

			DateFormat format = new SimpleDateFormat("HH'h'mm dd/MM/yy");
			
			if(p2.hasPlayedBefore()) {
				
				String nb = Connection.players.getString(p2.getUniqueId() + ".nb");
				
				if(nb.equalsIgnoreCase("1")) l.add(ChatColor.AQUA + "Il est le " + ChatColor.DARK_AQUA + Connection.players.getString(p2.getUniqueId() + ".nb") + ChatColor.AQUA + "er joueur à avoir rejoint le serveur.");
				else l.add(ChatColor.AQUA + "Il est le " + ChatColor.DARK_AQUA + Connection.players.getString(p2.getUniqueId() + ".nb") + ChatColor.AQUA + "ème joueur à avoir rejoint le serveur.");
				l.add(ChatColor.GOLD + "Première connexion : " + ChatColor.YELLOW + format.format(new Date(p2.getFirstPlayed())));
				l.add(ChatColor.GOLD + "Dernière connexion : " + ChatColor.YELLOW + format.format(new Date(p2.getLastPlayed())));
			}
			else l.add(ChatColor.GOLD + "Première connexion : " + ChatColor.YELLOW + "jamais");
			
			l.add(ChatColor.GOLD + "Unique ID : " + ChatColor.YELLOW + p2.getUniqueId());
			
			if(p2.isOnline()) l.add(ChatColor.GOLD + "Joueur en ligne ? " + ChatColor.GREEN + " Oui");
			else l.add(ChatColor.GOLD + "Joueur en ligne ? " + ChatColor.RED + " Non");
			
			if(pm.getPlugin("BanManager") != null) {				

				if(BanManager.getPlugin().isPlayerBanned(p2.getName())) l.add(ChatColor.GOLD + "Joueur banni ? " + ChatColor.RED + " Oui");
				else l.add(ChatColor.GOLD + "Joueur banni ? " + ChatColor.GREEN + " Non");
				
				if(BanManager.getPlugin().isPlayerMuted(p2.getName())) l.add(ChatColor.GOLD + "Joueur muté ? " + ChatColor.RED + " Oui");
				else l.add(ChatColor.GOLD + "Joueur muté ? " + ChatColor.GREEN + " Non");	
			}	
			else {
				
				if(p2.isBanned()) l.add(ChatColor.GOLD + "Joueur banni ? " + ChatColor.RED + " Oui");
				else l.add(ChatColor.GOLD + "Joueur banni ? " + ChatColor.GREEN + " Non");
			}
			if(Prison.prison(p2.getUniqueId())) l.add(ChatColor.GOLD + "Joueur emprisonné ? " + ChatColor.RED + " Oui");
			else l.add(ChatColor.GOLD + "Joueur emprisonné ? " + ChatColor.GREEN + " Non");
			
			s.sendMessage(l.toArray(new String[l.size()]));
			
			return true;
		}
		else if(label.equalsIgnoreCase("tpbed")) {
			
			if(!hasPerm(s, "sandboxcore.tpbed")) return true;
			
			if(!pl) s.sendMessage("Cette commande doit etre faites par un joueur");
			
			OfflinePlayer p2 = Bukkit.getOfflinePlayer(s.getName());
			
			if(args.length > 0) p2 = Bukkit.getOfflinePlayer(args[0]);
			
			if(HomeBed.getBed(p2.getUniqueId()) == null) {
				
				s.sendMessage(ChatColor.RED + "Lit introuvable.");
				return true;
			}
			
			((Player) s).teleport(HomeBed.getBed(p2.getUniqueId()));
			s.sendMessage(ChatColor.GREEN + "Tu viens de te tp sur le lit de " + ChatColor.DARK_GREEN + p2.getName());
			
			return true;
		}
		else s.sendMessage("Commande inconnue.");

		return false;
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void PlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {

        if(e.isCancelled()) return;

		if(e.getMessage().toLowerCase().length() >= 5 && e.getMessage().toLowerCase().substring(0, 5).equalsIgnoreCase("/info")) {

            e.setMessage("/sbinfo" + e.getMessage().substring(5));
        }

        String cmd = e.getMessage().toLowerCase().substring(1);

        if(cmd.startsWith("reload") || cmd.startsWith("rl") || cmd.startsWith("bukkit:reload") || cmd.startsWith("bukkit:rl")) {
            if(Bukkit.getServer().getIp().equalsIgnoreCase("46.105.208.141")) return;

            e.getPlayer().sendMessage(ChatColor.DARK_RED + "Espèce de noob ! On reload pas un serveur !");
            e.setCancelled(true);
        }
	}

    @EventHandler
    public void ServerCommand(ServerCommandEvent e) {

        if(Bukkit.getServer().getIp().equalsIgnoreCase("46.105.208.141")) return;

        String cmd = e.getCommand().toLowerCase();

        if(cmd.startsWith("reload") || cmd.startsWith("rl") || cmd.startsWith("bukkit:reload") || cmd.startsWith("bukkit:rl")) {

            e.getSender().sendMessage(ChatColor.DARK_RED + "Espèce de noob ! On reload pas un serveur !");
            e.setCommand("msg lafaipopo Un noob a essayé defaire un reload (bouuuhhh))");
        }
    }

	
	public static OfflinePlayer getPlayer(String pseudo) {
		
		if(Bukkit.getPlayerExact(pseudo) != null) return Bukkit.getPlayerExact(pseudo).getPlayer();
		else if(!players.contains(pseudo.toLowerCase())) return null;
		return Bukkit.getOfflinePlayer(UUID.fromString(players.getString(pseudo.toLowerCase())));
	}
	
	public static boolean hasPerm(CommandSender s, String perm) {
				
		if(s.hasPermission(perm)) return true;
		
		if(s instanceof Player) s.sendMessage(ChatColor.RED + "Tu n'as pas la permission (" + ChatColor.DARK_RED + perm + ChatColor.RED + ") pour exécuter cette commande.");
		else s.sendMessage("Tu n'as pas la permission (" + perm + ") pour executer cette commande.");
		
		return false;
	}
	
	public static boolean addFlags(final Flag<?> flag) {
					
		/*try {
			
			WGBukkit.getPlugin().getFlagRegistry().register(flag);
			
			return true;			
		} catch(NoSuchMethodError e) {
			*/
			try {
				
				Flag<?>[] flagArray = DefaultFlag.flagsList.clone();
				
				ArrayList<Flag<?>> flagList = new ArrayList<Flag<?>>(Arrays.asList(flagArray));
				
				flagList.add(flag);		
				flagArray = flagList.toArray(new Flag<?>[flagList.size()]);		
				
				Field f = DefaultFlag.class.getDeclaredField("flagsList");		
				
				f.setAccessible(true);
				
				int modifiers = f.getModifiers();
				Field modifierField = f.getClass().getDeclaredField("modifiers");
				modifiers = modifiers & ~Modifier.FINAL;
				modifierField.setAccessible(true);
				modifierField.setInt(f, modifiers); 
				
				f.set(DefaultFlag.flagsList, flagArray);
				f.setAccessible(false);
				
				for(final World w : Bukkit.getWorlds()) {
						
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

						@Override
						public void run() {
							
							try {
								
								WGBukkit.getRegionManager(w).load();
								
							} catch (StorageException e) {
								
								e.printStackTrace();
							}						
						}					
					}, 2L);
					
				}		
				
				return true;
				
			} catch(IllegalArgumentException | IllegalAccessException 
					| NoSuchFieldException | SecurityException ex) {
					
				ex.printStackTrace();
				return false;
			}
		//}
	}
	
	@SuppressWarnings("deprecation")
	static void setTabList(Player p) {
		
		String morts = ChatColor.DARK_GREEN + "Morts : " + ChatColor.GREEN + p.getStatistic(Statistic.DEATHS);
		String joueurs = ChatColor.DARK_GREEN + "Joueurs : " + ChatColor.GREEN + Bukkit.getOnlinePlayers().length;
		
		IChatBaseComponent header = ChatSerializer.a("{'text': '" + morts + "'}");
        IChatBaseComponent footer = ChatSerializer.a("{'text': '" + joueurs + "'}");
		
		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();

		try {
			Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, header);
            headerField.setAccessible(!headerField.isAccessible());

            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, footer);
            footerField.setAccessible(!footerField.isAccessible());
            
		} catch (Exception ex) {
    	
			ex.printStackTrace();
		}
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
	}
}