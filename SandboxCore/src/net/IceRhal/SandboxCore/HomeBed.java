package net.IceRhal.SandboxCore;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

public class HomeBed implements Listener {
	
	static ConfigManager cM;
	static FileConfiguration bed;
	
	@EventHandler
	public void PlayerEnterBed(PlayerBedEnterEvent e) {
		
		Location l = e.getPlayer().getLocation();
		
		String path = e.getPlayer().getUniqueId() + ".";
		
		bed.set(path + "world", l.getWorld().getName());
		bed.set(path + "x", l.getX());
		bed.set(path + "y", l.getY());
		bed.set(path + "z", l.getZ());
		cM.saveConfig("beds.yml");
	}
	
	static boolean load() {
		
		cM = new ConfigManager(Main.getPlugin());
		
		cM.saveDefaultConfig("beds.yml");
		
		bed = cM.getConfig("beds.yml");
		
		bed.options().copyDefaults(true);		
		cM.saveConfig("beds.yml");		
		
		return true;
	}
	
	static Location getBed(Player p) {
		
		return getBed(p.getUniqueId());
	}
	
	static Location getBed(UUID id) {
		
		String path = id + ".";
		
		if(bed.contains(path + "world")) {
			
			return new Location(Bukkit.getWorld(bed.getString(path + "world")), bed.getDouble(path + "x"),
					bed.getDouble(path + "y"), bed.getDouble(path + "z"));			
		}		
		else return null;
	}

}
