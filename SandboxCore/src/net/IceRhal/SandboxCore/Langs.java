package net.IceRhal.SandboxCore;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Langs {
	
	static String slot = ChatColor.RED + "Serveur plein.";
	static String firstJoinMessage = ChatColor.AQUA + "$player" + ChatColor.BLUE + " est  le " + ChatColor.AQUA + "$nbjoueur" + 
			ChatColor.BLUE + " ème joueur à avoir rejoint le serveur.";
	static String joinMessage = ChatColor.GOLD + "$player" + ChatColor.GREEN + " a rejoint le serveur !";
	static String quitMessage = ChatColor.GOLD + "$player" + ChatColor.GREEN + " a quitté le serveur.";
	static String motd = ChatColor.DARK_AQUA + "$playersonline" + ChatColor.AQUA + " / " + ChatColor.DARK_AQUA + "$maxplayers" +
			ChatColor.AQUA + " joueurs connectés sur le serveur : " + ChatColor.AQUA + "$list";
 	static String jailedMessage = ChatColor.RED + "Tu es emprisonné pour : " + ChatColor.DARK_RED + "$raison" + 
 			ChatColor.RED + ". Pendant encore " + ChatColor.DARK_RED + "$time" + ChatColor.RED + " minutes.";
 	static String unjailedMessage = ChatColor.GREEN + "Tu viens de sortir de prison !";
 	static String minuteJailed = ChatColor.RED + "Tu es en prison pour encore " + ChatColor.DARK_RED + "time" + 
 			ChatColor.RED + " minute(s).";
 	static String secondJailed = ChatColor.RED + "Tu es en prison pour encore " + ChatColor.DARK_RED + "time" + 
 			ChatColor.RED + " seconde(s).";
  	static String defaultRaison = ChatColor.RED + "une chose";
  	static String jailedPublicMessage = ChatColor.DARK_GREEN + "$player" + ChatColor.GREEN + " vient de se faire emprisonner.";
  	static String arretMessage = ChatColor.GREEN + "Arret du serveur dans " + ChatColor.DARK_GREEN + "$delay" +
  			ChatColor.GREEN + " seconde(s) pour :" + ChatColor.DARK_GREEN + "$arretRaison";
 	static String quitAFK = ChatColor.DARK_AQUA + "Tu es kick pour AFK";
  	static String kickAFK = ChatColor.DARK_AQUA + "$player" + ChatColor.AQUA + " est kick pour AFK.";
  	
  	static boolean loadConfig() {
  		
  		ConfigManager cm = new ConfigManager(Main.getPlugin());

  		cm.saveDefaultConfig("langs.yml");

  		FileConfiguration config = cm.getConfig("langs.yml");

  		slot = config.getString("server_full").replace("&", ChatColor.COLOR_CHAR + "");
  		firstJoinMessage = config.getString("firstJoinMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		joinMessage = config.getString("joinMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		quitMessage = config.getString("quitMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		motd = config.getString("motd").replace("&", ChatColor.COLOR_CHAR + "");
  		jailedMessage = config.getString("jailedMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		unjailedMessage = config.getString("unjailedMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		minuteJailed = config.getString("minuteActionBarMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		secondJailed = config.getString("secondActionBarMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		defaultRaison = config.getString("defaultRaison").replace("&", ChatColor.COLOR_CHAR + "");
  	    jailedPublicMessage = config.getString("jailedPublicMessage").replace("&", ChatColor.COLOR_CHAR + "");
  	    arretMessage = config.getString("arretMessage").replace("&", ChatColor.COLOR_CHAR + "");
  		quitAFK = config.getString("quitAFK").replace("&", ChatColor.COLOR_CHAR + "");
  		kickAFK = config.getString("kickAFK").replace("&", ChatColor.COLOR_CHAR + "");

  		return true;
  	}
}