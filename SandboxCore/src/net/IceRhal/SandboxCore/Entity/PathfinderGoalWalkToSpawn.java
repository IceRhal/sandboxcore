package net.IceRhal.SandboxCore.Entity;

import net.minecraft.server.v1_8_R1.EntityCreature;
import net.minecraft.server.v1_8_R1.PathfinderGoal;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

public class PathfinderGoalWalkToSpawn extends PathfinderGoal {

	float speed;
	private EntityCreature ent;
	
	public PathfinderGoalWalkToSpawn(EntityCreature entitycreature, float speed) {
		
		this.speed = speed;
		this.ent = entitycreature;	
	}
	 	 
	@SuppressWarnings("deprecation")
	public boolean a() {
				
		return true;
	}
	
	 public boolean b() {
		 		 
		 return !ent.getNavigation().m();
	}
	 	
	@SuppressWarnings("deprecation")
	public void c() {
				
		Location l = ent.world.getWorld().getSpawnLocation();
		
		ent.getNavigation().a(l.getX(), l.getY() + 1, l.getZ(), speed);
	}
}
