package net.IceRhal.SandboxCore.Entity;

import java.lang.reflect.Field;
import java.util.Random;

import net.minecraft.server.v1_8_R1.EntityCow;
import net.minecraft.server.v1_8_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_8_R1.World;
import org.bukkit.craftbukkit.v1_8_R1.util.UnsafeList;

public class IceCow extends EntityCow {
	
	boolean creeper = false;
	
    public IceCow(World world) {
    	
		super(world);
		if(new Random().nextInt(100) == 1) {

            creeper = true;

            try {

                Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
                bField.setAccessible(true);
                Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
                cField.setAccessible(true);
                bField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
                bField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
                cField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
                cField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());

            } catch (Exception exc) {

                exc.printStackTrace();
            }

            this.goalSelector.a(0, new PathfinderGoalFloat(this));
            this.goalSelector.a(3, new PathfinderGoalWalkToSpawn(this, 1.0F));

        }
    }
    
    @Override
    protected String z() {

        if(creeper) return "mob.creeper.say";
    	else return "mob.cow.say";
    }
}

