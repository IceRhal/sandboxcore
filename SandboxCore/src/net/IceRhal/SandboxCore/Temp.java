package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Temp implements CommandExecutor, TabCompleter, Listener {

	FileConfiguration players = Main.players;
	ConfigManager cM = Main.cm;
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		
		boolean pl = false;
		
		if(s instanceof Player) pl = true;
			
		if(label.equalsIgnoreCase("temp")) {
			
			if(!Main.hasPerm(s, "sandboxcore.temp")) return true;
			
			if(args.length == 3) {
				
				OfflinePlayer p2 = Bukkit.getOfflinePlayer(args[0]);
				
				if(p2 != null && p2.hasPlayedBefore()) {
					
					String g = null;
					
					for(String grp : PermissionsEx.getPermissionManager().getGroupNames()) {
						
						if(grp.equalsIgnoreCase(args[1])) g = grp;
					}
					
					if(g != null) {
						
						int time = 0;
						
						try {
							
							time = Integer.parseInt(args[2]);
							
							if(time < 0) time = 0;							
						} catch (NumberFormatException e) {
							
							if(pl) s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/temp [player] [group] [time]");
							else s.sendMessage("Erreur dans la commande. /temp [player] [group] [time]");
							return true;
						}
						
						Date d = new Date();
						d.setHours(d.getHours() + 24 * time);
						
						players.set(p2.getUniqueId() + "." + g, d.getTime());
						if(time == 0) players.set(p2.getUniqueId() + "." + g, null);						
						cM.saveConfig("players.yml");
						
						if(time == 0) {
							
							if(pl) s.sendMessage(ChatColor.GREEN + "Le groupe " + ChatColor.DARK_GREEN 
									+ g + ChatColor.GREEN + " est retiré pour " + ChatColor.DARK_GREEN + p2.getName());
							else s.sendMessage("Le groupe " + g + " est retiré pour " + p2.getName());
						}
						else {
							
							if(p2.isOnline()) {
								
								PermissionsEx.getUser(p2.getName()).addGroup(g);
								Bukkit.getPlayer(p2.getUniqueId()).sendMessage(ChatColor.AQUA + "Tu deviens " + ChatColor.DARK_AQUA + g
										+ ChatColor.AQUA + " pendant " + ChatColor.DARK_AQUA + time + ChatColor.AQUA + " jour(s).");
							}
							
							if(pl) s.sendMessage(ChatColor.GREEN + "Le groupe " + ChatColor.DARK_GREEN 
									+ g + ChatColor.GREEN + " est défini pour " + ChatColor.DARK_GREEN + p2.getName() 
									+ ChatColor.GREEN + " pendant " + ChatColor.DARK_GREEN + time + ChatColor.GREEN + " jour(s).");
							else s.sendMessage("Le groupe " + g + " est défini pour " + p2.getName() + " pendant " + time + " jour(s).");
						}
					}
					else if(pl) s.sendMessage(ChatColor.RED + "Groupe " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " inconnu.");
					else s.sendMessage("Groupe " + args[1] + " inconnue.");
				}
			}
			else if(pl) s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/temp [player] [group] [time]");
			else s.sendMessage("Erreur dans la commande. /temp [player] [group] [time]");
			
			return true;
		}
		else if(label.equalsIgnoreCase("vip") && pl) {
			
			Player p = (Player) s;
			HashMap<String, Integer> grp = new HashMap<String, Integer>();
			int j;
			
			for(String g : PermissionsEx.getUser(p).getGroupsNames()) {
				
				if(players.contains(p.getUniqueId() + "." + g)) {	
					
					j = new Date(players.getLong(p.getUniqueId() + "." + g) - new Date().getTime()).getDay();
					
					grp.put(g, j);					
				}
			}
			
			if(grp.isEmpty()) p.sendMessage(ChatColor.AQUA + "Tu n'es pas VIP.");
			else {
				
				for(String g : grp.keySet()) {
					
					if(grp.get(g) == 1) p.sendMessage(ChatColor.AQUA + "Tu es " + ChatColor.DARK_AQUA + g 
							+ ChatColor.AQUA + " pendant encore " + ChatColor.DARK_AQUA + grp.get(g) + ChatColor.AQUA + " jour.");
					else if(grp.get(g) > 1) p.sendMessage(ChatColor.AQUA + "Tu es " + ChatColor.DARK_AQUA + g 
							+ ChatColor.AQUA + " pendant encore " + ChatColor.DARK_AQUA + grp.get(g) + ChatColor.AQUA + " jours.");
					else if(grp.get(g) <= 0) p.sendMessage(ChatColor.AQUA + "Tu es " + ChatColor.DARK_AQUA + g 
							+ ChatColor.AQUA + " pendant encore moins d'un jour.");
				}
			}
			
			return true;
		}
		
		return false;
	}


	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		ArrayList<String> s = new ArrayList<String>();
		
		if(label.equalsIgnoreCase("temp")) {
			
			if(args.length == 1) {
				
				for(OfflinePlayer p2 : Bukkit.getOfflinePlayers()) {
					
					if(p2 != null && p2.hasPlayedBefore() && !s.contains(p2.getName()) 
							&& p2.getName().toLowerCase().startsWith(args[0].toLowerCase())) s.add(p2.getName());
					if(s.size() >= 10) break;
				}
			}
			else if(args.length == 2) {
				
				for(String grp : PermissionsEx.getPermissionManager().getGroupNames()) {
					
					if(!s.contains(grp) && grp.toLowerCase().startsWith(args[1].toLowerCase())) s.add(grp);
					if(s.size() >= 10) break;					
				}
 			}
		}
		return s;
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void PlayerJoin(PlayerJoinEvent e) {
		
		Player p = e.getPlayer();
				
		for(String g : PermissionsEx.getPermissionManager().getGroupNames()) {
			
			if(players.contains(p.getUniqueId() + "." + g)) {
				
				long nb = players.getLong(p.getUniqueId() + "." + g) - new Date().getTime();
				int j = (int) nb/(36000 *24);
				
				if(nb > 0) {
					
					PermissionsEx.getUser(p).addGroup(g);
					
					if(j > 0) p.sendMessage(ChatColor.AQUA + "Tu es " + ChatColor.DARK_AQUA + g + ChatColor.AQUA + " pendant encore " + ChatColor.DARK_AQUA + j
							+ ChatColor.AQUA + " jour(s).");
					else p.sendMessage(ChatColor.AQUA + "Tu es " + ChatColor.DARK_AQUA + g + ChatColor.AQUA + " pendant encore moins d'un jour.");
				}
				else {
					
					p.sendMessage(ChatColor.AQUA + "Tu n'es plus " + ChatColor.DARK_AQUA + g);
					players.set(p.getUniqueId() + "." + g, null);
					cM.saveConfig("players.yml");
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlayerQuit(PlayerQuitEvent e) {
		
		Player p = e.getPlayer();
		
		for(String g : PermissionsEx.getUser(p).getGroupsNames()) {
			
			if(players.contains(p.getUniqueId() + "." + g)) {
				
				PermissionsEx.getUser(p).removeGroup(g);
			}
		}
	}
}
