package net.IceRhal.SandboxCore;

import org.bukkit.Location;
import org.bukkit.TravelAgent;
import org.bukkit.World.Environment;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class NetherRatio implements Listener {
	
	static ConfigManager cm;
	static FileConfiguration config;
	static int var;
	
	@EventHandler
	public void PlayerPortal(PlayerPortalEvent e) {
		
		if(e.getCause() == TeleportCause.NETHER_PORTAL) {
			
			Location l1 = e.getFrom();
			Location l2 = e.getTo();
			
			double x = l1.getX();
			double z = l1.getZ();
			
			TravelAgent agent = e.getPortalTravelAgent();
			
			if(l1.getWorld().getEnvironment() == Environment.NORMAL) {
				
				x /= var;
				z /= var;
			}
			else if(l1.getWorld().getEnvironment() == Environment.NETHER) {

				x *= var;
				z *= var;
			}
			else return;			

			l2.setX(x);
			l2.setZ(z);
			agent.findOrCreate(l2);
		}
	}
	
	@EventHandler
	public void EntityPortal(EntityPortalEvent e) {
		
		if(e.getTo().getWorld().getEnvironment() == Environment.NETHER 
				|| e.getFrom().getWorld().getEnvironment() == Environment.NETHER) {
			
			Location l1 = e.getFrom();
			Location l2 = e.getTo();
			
			double x = l1.getX();
			double z = l1.getZ();
			
			TravelAgent agent = e.getPortalTravelAgent();
			
			if(l1.getWorld().getEnvironment() == Environment.NORMAL) {
				
				x /= var;
				z /= var;
			}
			else if(l1.getWorld().getEnvironment() == Environment.NETHER) {

				x *= var;
				z *= var;
			}
			else return;			

			l2.setX(x);
			l2.setZ(z);
			agent.findOrCreate(l2);
		}
	}
	
	static boolean loadConfig() {
		
		cm = new ConfigManager(Main.plugin);
		
		cm.saveDefaultConfig("ratio.yml");
		
		config = cm.getConfig("ratio.yml");
				
		try {
			
			var = config.getInt("ratio");
		} catch (NumberFormatException | NullPointerException e) {
		
			var = 8;
		}
		
		return true;
	}

}
