package net.IceRhal.SandboxCore;

import java.util.Arrays;
import java.util.Iterator;

import net.IceRhal.SandboxCore.Chat.ChatAssembler;
import net.IceRhal.SandboxCore.Chat.ClickAction;
import net.IceRhal.SandboxCore.Chat.ClickModifier;
import net.IceRhal.SandboxCore.Chat.CustomChar;
import net.IceRhal.SandboxCore.Chat.CustomString;
import net.IceRhal.SandboxCore.Chat.HoverAction;
import net.IceRhal.SandboxCore.Chat.HoverModifier;
import net.IceRhal.SandboxCore.Chat.Modifier;
import net.IceRhal.SandboxCore.Chat.ModifierType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Staff implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {

		if(label.equalsIgnoreCase("staff") || label.equalsIgnoreCase("s")) {
			
			if(!Main.hasPerm(s, "sandboxcore.staff.send")) return true;
			
			if(args.length == 0) {
				
				s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/staff [message]");
				return true;
			}
			
			String msg = "";
			
			Iterator<String> it = Arrays.asList(args).iterator();
			
			while(it.hasNext()) {
				
				msg += it.next();
				if(it.hasNext()) msg += " ";
			}
			
			for(Player p : Bukkit.getOnlinePlayers()) {
				
				if(p.hasPermission("sandboxcore.staff.view")) {
					
					if(s instanceof Player && PermissionsEx.getUser(s.getName()).getOption("staff") != null) new ChatAssembler(new CustomChar(new CustomString(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + s.getName() + " "), ChatColor.YELLOW + "[" 
							+ ChatColor.GOLD + s.getName() + ChatColor.YELLOW + "] " ).addModifier(new HoverModifier(HoverAction.SHOW_TEXT, PermissionsEx.getUser(s.getName()).getOption("staff").replace("&", ChatColor.COLOR_CHAR + ""))),
							new CustomString(new Modifier(ModifierType.COLOR, "yellow"), msg))).send(p); 							
					else new ChatAssembler(new CustomChar(new CustomString(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + s.getName() + " "), ChatColor.YELLOW + "[" 
							+ ChatColor.GOLD + s.getName() + ChatColor.YELLOW + "] " ),
							new CustomString(new Modifier(ModifierType.COLOR, "yellow"), msg))).send(p);
					
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
				}
			}
			
			Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "(" + ChatColor.GOLD + s.getName() + ChatColor.YELLOW + ") " + msg);			
			
			return true;
		}
		else return false;		
	}
}
