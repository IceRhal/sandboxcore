package net.IceRhal.SandboxCore;

import org.bukkit.ChatColor;
import org.bukkit.World.Environment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public class Wither implements Listener {
	
	@EventHandler
	public void CreatureSpawn(CreatureSpawnEvent e) {
				
		if(e.getSpawnReason() == SpawnReason.BUILD_WITHER) {
						
			if(e.getEntity().getWorld().getEnvironment() == Environment.NORMAL || e.getEntity().getWorld().getEnvironment() == Environment.THE_END) {
				
				for(Entity ent : e.getEntity().getNearbyEntities(10, 10, 10)) {
					
					if(ent instanceof Player) {
						
						((Player) ent).sendMessage(ChatColor.DARK_RED + "Les withers ne sont pas autorisés dans ce monde.");
						((Player) ent).sendMessage(ChatColor.DARK_RED +	"Ils sont autorisés seulement dans le nether.");
					}					
				}
				e.setCancelled(true);
			}
		}
		
	}

}
