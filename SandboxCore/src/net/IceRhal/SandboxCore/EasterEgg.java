package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class EasterEgg implements Listener {	
	
	@EventHandler
	public void PlayerInteractEntity(PlayerInteractEntityEvent e) {		

		Player ice = Bukkit.getPlayer(UUID.fromString("bad68c1b-207a-41f4-898b-5b09b41af7cf"));
		ItemStack hand = e.getPlayer().getItemInHand();
			
		if(e.getRightClicked() == ice && hand.getType() == Material.BUCKET) {
			
			PlayerInventory inv = e.getPlayer().getInventory();
			
			if(hand.getAmount() > 1) hand.setAmount(hand.getAmount() - 1);
			else inv.remove(hand);
			
			e.getPlayer().getWorld().dropItem(e.getPlayer().getLocation(), milk());			
		}
		
		/*if(e.getPlayer() == ice) {
			
			if(e.getPlayer().isSneaking()) {
				
				if(e.getRightClicked() instanceof Villager) {
					
					e.setCancelled(true);
					
					InventorySubcontainer inv = ((CraftVillager) e.getRightClicked()).getHandle().inventory;
					
					try {
										
						Field f = inv.getClass().getDeclaredField("b");
						
						f.setAccessible(true);
						
						int modifiers = f.getModifiers();
						Field modifierField = f.getClass().getDeclaredField("modifiers");
						modifiers = modifiers & ~Modifier.PRIVATE;
						modifierField.setAccessible(true);
						modifierField.setInt(f, modifiers); 

						f.set(inv, 9);
						
						f.setAccessible(false);
						
						inv.items = Arrays.asList(inv.items).toArray(new net.minecraft.server.v1_8_R1.ItemStack[9]);
										
					} catch(IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
						ex.printStackTrace();
					}

					ice.openInventory(((CraftVillager) e.getRightClicked()).getInventory());
				}				
				else if(e.getRightClicked() instanceof Player) {
					
					e.getPlayer().openInventory(((Player)e.getRightClicked()).getInventory());
					e.getPlayer().sendMessage(ChatColor.YELLOW + "Tu as ouvert l'inventaire de " + ChatColor.GOLD + ((Player)e.getRightClicked()).getName());
				}
			}	
		}*/
	}
	
	@EventHandler
	public void EntityDamage(EntityDamageEvent e) {
		
		if(e.getEntity() == Bukkit.getPlayer(UUID.fromString("bad68c1b-207a-41f4-898b-5b09b41af7cf"))) {
			
			e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.COW_HURT, 1, 1);
		}
	}
	
	ItemStack milk() {
		
		ItemStack is = new ItemStack(Material.MILK_BUCKET, 1);
		
		ItemMeta im = is.getItemMeta();
		
		im.setDisplayName(ChatColor.ITALIC + "Lait d'IceRhal");
		
		ArrayList<String> lore = new ArrayList<String>();
		
		lore.add(ChatColor.GOLD + "Apparement ceci est du lait.");
		lore.add(ChatColor.GOLD + "A moins que ...");
		im.setLore(lore);
		
		is.setItemMeta(im);
		return is;
	}
}
