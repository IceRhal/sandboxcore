package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import net.IceRhal.SandboxCore.Chat.ChatAssembler;
import net.IceRhal.SandboxCore.Chat.ClickAction;
import net.IceRhal.SandboxCore.Chat.ClickModifier;
import net.IceRhal.SandboxCore.Chat.CustomChar;
import net.IceRhal.SandboxCore.Chat.CustomString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Message implements CommandExecutor {

	static HashMap<String, String> last = new HashMap<String, String>();
	static ArrayList<String> off = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {

		if(label.equalsIgnoreCase("tell") || label.equalsIgnoreCase("msg") || label.equalsIgnoreCase("mp")
				|| label.equalsIgnoreCase("m")) {
			
			if(!Main.hasPerm(s, "sandboxcore.message")) return true;
			
			if(args.length < 2) {
				
				if(args.length == 1 && args[0].equalsIgnoreCase("off") && s.hasPermission("sandboxcore.message.off")) {
					
					String n = s.getName().toLowerCase();
					
					if(off.contains(n)) {
						
						s.sendMessage(ChatColor.GREEN + "Tu autorise désormais les autres personnes à t'envoyer des messages privés.");
						off.remove(n);
						return true;
					}
					
					s.sendMessage(ChatColor.AQUA + "Les joueurs ne peuvent désormais plus t'envoyer des messages privés.");
					off.add(n);
					return true;
				}
				
				s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/" + label.toLowerCase() + " [joueur] [message]");
				return true;
			}
			
			Player p = Bukkit.getPlayer(args[0]);
			
			if(p == null && !args[0].equalsIgnoreCase("console")) {
				
				s.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[0] + ChatColor.RED + " inconnue.");
				return true;
			}
			
			if(off.contains(args[0].toLowerCase()) && !s.hasPermission("sandboxcore.message.off.bypass")) {
				
				s.sendMessage(ChatColor.RED + "Ce joueur ne souhaite pas recevoir de message privé.");
				return true;
			}
			
			last.put(args[0].toLowerCase(), s.getName().toLowerCase());
			last.put(s.getName().toLowerCase(), args[0].toLowerCase());
			
			String msg = "";
			
			Iterator<String> it = Arrays.asList(args).iterator();
			
			it.next();
			
			while(it.hasNext()) {
				
				String str = it.next();
				
				msg += " " + str;
			}
			
			Player p1 = null;
			if(s instanceof Player) p1 = (Player) s;
			
			String pName1 = ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à ";
			String pName2;
						
			if(args[0].equalsIgnoreCase("console")) {
				
				System.out.print(s.getName() + " a Console :" + msg);
				
				if(s != null && s instanceof Player) {
					
					pName1 += "Console";
					pName2 = "Console";
										
					ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
							.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
							new CustomString(msg)));
														
					((CraftPlayer) p1).getHandle().playerConnection.sendPacket(c.getPacket());
				}				
				else s.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à Console :" + ChatColor.RESET + msg);
				
				return true;
			}
			
			pName1 += p.getName();
			pName2 = p.getName();
			
			if(s != null && s instanceof Player) {
				
				ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
					.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
					new CustomString(msg)));
				
				((CraftPlayer) p1).getHandle().playerConnection.sendPacket(c.getPacket());
			}
			else s.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à " + p.getName() + " :" + ChatColor.RESET + msg);			
			
			ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
				.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
				new CustomString(msg)));
			
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(c.getPacket());
			
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			return true;
		}
		else if(label.equalsIgnoreCase("r")) {
			
			if(!Main.hasPerm(s, "sandboxcore.message")) return true;
			
			if(args.length < 1) {
				
				s.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/r [message]");
				return true;
			}
						
			if(!last.containsKey(s.getName().toLowerCase())) {
				
				s.sendMessage(ChatColor.RED + "Tu ne peux répondre à personne.");
				return true;
			}
						
			String rep = last.get(s.getName().toLowerCase());
			
			if(off.contains(rep.toLowerCase()) && !s.hasPermission("sandboxcore.message.off.bypass")) {
				
				s.sendMessage(ChatColor.RED + "Ce joueur ne souhaite pas recevoir de message privé.");
				return true;
			}			
			
			last.put(rep.toLowerCase(), s.getName().toLowerCase());
			last.put(s.getName().toLowerCase(), rep.toLowerCase());
			
			String msg = "";
			
			Iterator<String> it = Arrays.asList(args).iterator();
						
			while(it.hasNext()) {
				
				String str = it.next();
				
				msg += " " + str;
			}
			
			Player p1 = null;
			if(s instanceof Player) p1 = (Player) s;
			
			String pName1 = ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à ";
			String pName2;
						
			if(rep.equalsIgnoreCase("console")) {
				
				System.out.print(s.getName() + " a Console :" + msg);
				
				if(s != null && s instanceof Player) {
					
					pName1 += "Console";
					pName2 = "Console";
					
					ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
						.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
						new CustomString(msg)));
					
					((CraftPlayer) p1).getHandle().playerConnection.sendPacket(c.getPacket());
				}				
				else s.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à Console :" + ChatColor.RESET + msg);
				return true;
			}
			
			Player p = Bukkit.getPlayer(rep);
			
			if(p == null) {
				
				s.sendMessage(ChatColor.RED + "Le dernier joueur a qui tu as parlé s'est déconnecté.");
				return true;
			}
			
			pName1 += p.getName();
			pName2 = p.getName();
			
			if(s != null && s instanceof Player) {
							
				ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
					.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
					new CustomString(msg)));
				
				((CraftPlayer) p1).getHandle().playerConnection.sendPacket(c.getPacket());
			}
			else s.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + s.getName() + " à " + p.getName() + " :" + ChatColor.RESET + msg);			
			
			ChatAssembler c = new ChatAssembler(new CustomChar(new CustomString(pName1 + " :")
				.addModifier(new ClickModifier(ClickAction.SUGGEST_COMMAND, "/msg " + pName2 + " ")),
				new CustomString(msg)));	
			
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(c.getPacket());			
			
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			return true;
		}
		return false;
	}

}
