package net.IceRhal.SandboxCore;

import java.io.*;
import java.util.logging.Level;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.plugin.java.JavaPlugin;

public class ConfigManager
{
	
    private JavaPlugin plugin;
    private FileConfiguration customConfig;
    private File customConfigFile;

    public ConfigManager(JavaPlugin plugin)
    {
        customConfig = null;
        customConfigFile = null;
        this.plugin = plugin;
    }

    public void reloadConfig(String fileName)
    {
        if(customConfigFile == null) customConfigFile = new File(plugin.getDataFolder(), fileName);
        
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
        Reader defConfigStream;
        
        try {
        	
            try {
            	
                defConfigStream = new InputStreamReader(plugin.getResource(fileName), "UTF-8");
            } catch(NullPointerException e) {
            	
                plugin.saveResource(fileName, true);
                return;
            }
        } catch(UnsupportedEncodingException e) {
        	
            defConfigStream = null;
        }
        
        if(defConfigStream != null) {
        	
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            customConfig.setDefaults(defConfig);
        }
    }

    public FileConfiguration getConfig(String fileName) {
    	
        if(customConfig == null) reloadConfig(fileName);
        return customConfig;
    }

    public void saveConfig(String fileName) {
    	
        if(customConfig == null || customConfigFile == null) return;
        
        try {
        	
        	getConfig(fileName).save(customConfigFile);
        } catch(IOException|NullPointerException ex) {
        	
            plugin.getLogger().log(Level.SEVERE, ("Could not save config to " + customConfigFile), ex);
        }
    }

    public void saveDefaultConfig(String fileName)
    {
        if(customConfigFile == null) customConfigFile = new File(plugin.getDataFolder(), fileName);
        
        if(!customConfigFile.exists()) plugin.saveResource(fileName, false);
        
        saveConfig(fileName);
    }
}
