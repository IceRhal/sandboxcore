package net.IceRhal.SandboxCore;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class AFK extends BukkitRunnable {

	HashMap<Player, Float[]> pLast = new HashMap<Player, Float[]>();
	HashMap<Player, Integer> AFKTime = new HashMap<Player, Integer>();
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
				
		for(Player p : Bukkit.getOnlinePlayers()) {
			
			if(p.hasPermission("afk.bypass")) continue;
					
			Float[] f = new Float[2];
			
			f[0] = p.getLocation().getYaw();
			f[1] = p.getLocation().getPitch();			
			
			if(!pLast.containsKey(p)) {
				
				pLast.put(p, f);
				continue;
			}
			else if(pLast.get(p)[0].doubleValue() != f[0].doubleValue() 
					&& pLast.get(p)[1].doubleValue() != f[1].doubleValue()) {
				
				pLast.put(p, f);
				if(AFKTime.containsKey(p)) AFKTime.put(p, 0);
				continue;
			}
			else {
				
				if(AFKTime.containsKey(p)) {
										
					AFKTime.put(p, 1 + AFKTime.get(p));
				}
				else {
					
					AFKTime.put(p, 1);
				}
				
				if(p.hasPermission("afk.time." + AFKTime.get(p))) {
					
					pLast.put(p, null);
					AFKTime.put(p, null);
					
					Bukkit.broadcastMessage(Langs.kickAFK.replace("$player", p.getName()));
					p.kickPlayer(Langs.quitAFK.replace("$player", p.getName()));
					continue;
				}
			}			
		}
	}
}
