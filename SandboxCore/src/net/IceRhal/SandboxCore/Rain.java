package net.IceRhal.SandboxCore;

import java.util.Random;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class Rain implements Listener {
	
	static double multiplier = 1;
	static ConfigManager cm;
	static FileConfiguration config;
	
	@EventHandler
	public void WeatherChange(WeatherChangeEvent e) {
		
		if(!e.toWeatherState()) {
			
			e.getWorld().setWeatherDuration((int) (new Random(168000).nextInt() + 12000 * multiplier));
		}
	}
	
	static boolean loadConfig() {
		
		cm = new ConfigManager(Main.getPlugin());

		cm.saveDefaultConfig("rain.yml");

		config = cm.getConfig("rain.yml");
		
		cm.saveConfig("rain.yml");
		
		if(config.contains("multiplier")) multiplier = config.getDouble("multiplier");
		
		return true;
	}

}
