package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class Warp implements CommandExecutor, TabCompleter {
	
	static ConfigManager cm;
	static FileConfiguration config;
	static HashMap<String, Location> warps = new HashMap<String, Location>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(label.equalsIgnoreCase("warp")) {
				
				if(!Main.hasPerm(p, "sandboxcore.warp")) return true;
				
				if(args.length == 0) {
					
					p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/warp [warp]");
					return true;
				}
				
				if(!warps.containsKey(args[0].toLowerCase())) {
					
					p.sendMessage(ChatColor.RED + "Warp " + ChatColor.DARK_RED + args[0] + ChatColor.RED + " introuvable.");
					return true;
				}
				
				p.teleport(warps.get(args[0].toLowerCase()), TeleportCause.COMMAND);
				p.getWorld().playSound(warps.get(args[0].toLowerCase()), Sound.ENDERMAN_TELEPORT, 1, 1);
				p.sendMessage(ChatColor.GREEN + "Tu viens de te téléporter au warp " + ChatColor.DARK_GREEN + maj(args[0]) + ChatColor.GREEN + " !");				
			
				Bukkit.broadcast(ChatColor.GOLD + p.getName() + ChatColor.YELLOW + " vient de se téléporter au warp " + ChatColor.GOLD + maj(args[0]), "sandboxcore.warp.broadcast");
			}
			else if(label.equalsIgnoreCase("warps")) {
				
				if(!Main.hasPerm(p, "sandboxcore.warp")) return true;
				
				if(warps.isEmpty()) {
					
					p.sendMessage(ChatColor.GREEN + "Aucun warp existe.");
					return true;
				}
				
				String warp = "";
				
				Iterator<String> it = warps.keySet().iterator();
				
				while(it.hasNext()) {
					
					String s = ChatColor.DARK_GREEN + maj(it.next()) + ChatColor.GREEN;
					
					if(it.hasNext()) warp += s + ", ";
					else warp += s + ".";
				}
				
				p.sendMessage(ChatColor.GREEN + "Liste des warps : " + warp);
			}
			else if(label.equalsIgnoreCase("setwarp")) {
				
				if(!Main.hasPerm(p, "sandboxcore.setwarp")) return true;
				
				if(args.length == 0) {
					
					p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/setwarp [warp]");
					return true;
				}
				
				String path = args[0].toLowerCase() + ".";
				Location loc = p.getLocation();
				
				config.set(path + "x", loc.getX());
				config.set(path + "y", loc.getY());
				config.set(path + "z", loc.getZ());
				config.set(path + "yaw", loc.getYaw());
				config.set(path + "pitch", loc.getPitch());
				config.set(path + "world", loc.getWorld().getName());
				cm.saveConfig("warps.yml");
				
				warps.put(args[0].toLowerCase(), loc);
				
				p.sendMessage(ChatColor.GREEN + "Tu viens de définir la position du warp " + ChatColor.DARK_GREEN + maj(args[0]) 
						+ ChatColor.GREEN + " là ou tu te trouve.");
			}
			else if(label.equalsIgnoreCase("delwarp")) {
				
				if(!Main.hasPerm(p, "sandboxcore.setwarp")) return true;
				
				if(args.length == 0) {
					
					p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/delwarp [warp]");
					return true;
				}
				
				if(!warps.containsKey(args[0].toLowerCase()) || !config.contains(args[0].toLowerCase() + ".world")) {
					
					p.sendMessage(ChatColor.GREEN + "Le warp " + ChatColor.DARK_GREEN + maj(args[0]) + ChatColor.GREEN + " n'existe déjà pas.");
					return true;
				}
				
				config.set(args[0].toLowerCase(), null);
				cm.saveConfig("warps.yml");
				
				warps.remove(args[0].toLowerCase());
				
				p.sendMessage(ChatColor.GREEN + "Le warp " + ChatColor.DARK_GREEN + maj(args[0]) + ChatColor.GREEN + " est supprimé.");				
			}
			else p.sendMessage(ChatColor.RED + "Commande inconnue.");
			
			return true;
		}
		else sender.sendMessage("Cette commande doit être faite par un joueur.");
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		ArrayList<String> s = new ArrayList<String>();
		
		if(label.equalsIgnoreCase("delwarp") || label.equalsIgnoreCase("warp")) {
			
			for(String warp : warps.keySet()) {
				
				if(warp.toLowerCase().startsWith(args[0].toLowerCase())) s.add(maj(warp)); 
				
				if(s.size() >= 10) break;
			}
		}
		
		return s;
	}
	
	static public boolean loadConfig() {
		
		cm = new ConfigManager(Main.getPlugin());

		cm.saveDefaultConfig("warps.yml");

		config = cm.getConfig("warps.yml");
		
		for(String key : config.getKeys(true)) {
			
			String[] k = key.split("[.]");
			
			if(k.length != 1 && (k.length > 0 && !config.contains(k[0] + ".world"))) continue;
			
			warps.put(k[0].toLowerCase(), new Location(Bukkit.getWorld(config.getString(k[0] + ".world")),
					config.getInt(k[0] + ".x"), config.getInt(k[0] + ".y"), config.getInt(k[0] + ".z"),
					(float) config.getDouble(k[0] + ".yaw"), (float) config.getDouble(k[0] + ".pitch")));
		}
		
		return true;
	}
	
	String maj(String s) {
		
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

}
