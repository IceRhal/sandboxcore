package net.IceRhal.SandboxCore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class EnPrison {
	
	static ArrayList<UUID> prison = new ArrayList<UUID>();
	static FileConfiguration players = Prison.players;
	static FileConfiguration config = Prison.config;
	static ConfigManager cm = Prison.cm;
	static ConfigManager cmP = Main.cm;
	static HashMap<Player, Integer> task = new HashMap<Player, Integer>();
    static HashMap<Player, Integer> task2 = new HashMap<Player, Integer>();

	static void put(final Player p) {
		
		int x = 0;
		final UUID i = p.getUniqueId();

		if(!players.contains(i + ".loc.world")) {
			
			for(ItemStack is : p.getInventory().getArmorContents()) {
				
				players.set(i + ".armor." + x, is);
				x++;
			}

			x = 0;

			for(ItemStack is : p.getInventory().getContents()) {
				
				players.set(i + ".stuff." + x, is);
				x++;
			}

			Location l = p.getLocation();

			players.set(i + ".loc.x", Double.valueOf(l.getX()));
			players.set(i + ".loc.y", Double.valueOf(l.getY()));
			players.set(i + ".loc.z", Double.valueOf(l.getZ()));
			players.set(i + ".loc.yaw", Float.valueOf(l.getYaw()));
			players.set(i + ".loc.pitch", Float.valueOf(l.getPitch()));
			players.set(i + ".loc.world", l.getWorld().getName());
			
			players.set(i + ".vie", Double.valueOf(((Damageable)p).getHealth()));
			players.set(i + ".food", Integer.valueOf(p.getFoodLevel()));
			players.set(i + ".saturation", Float.valueOf(p.getSaturation()));
			players.set(i + ".exp", Float.valueOf(p.getExp()));
			players.set(i + ".lvl", Integer.valueOf(p.getLevel()));

			cmP.saveConfig("players.yml");			
		}
		
		final String raison = players.getString(p.getUniqueId() + ".raison").replace("&", ChatColor.COLOR_CHAR + "");
		Long time = Long.valueOf(players.getLong(i + ".jail"));		

		if(time == 0L) return;
		
		prison.add(p.getUniqueId());
		
		if(config.contains("world")) p.teleport(new Location(Bukkit.getWorld(config.getString("world")),
				config.getDouble("x"),
				config.getDouble("y"),
				config.getDouble("z"),
				(float)config.getLong("yaw"), 
				(float)config.getLong("pitch")));

		p.setHealth(20.0D);
		p.setSaturation(20.0F);
		p.setFoodLevel(20);
		p.setExp(0.0F);
		p.setLevel(0);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);

		long min = (time - time % 60000L) / 60000;

		if(time.longValue() % 60000L != 0L) min++;

		final HashMap<Player, Float> pitch = new HashMap<Player, Float>();
		final HashMap<Player, Float> yaw = new HashMap<Player, Float>();
		final HashMap<Player, Integer> tickAFK = new HashMap<Player, Integer>();

		task.put(p, Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
			
			@Override
			public void run() {

                if(!p.isOnline()) Bukkit.getScheduler().cancelTask(task.get(p));

                Long time = Long.valueOf(players.getLong(i + ".jail"));

                if(time.longValue() <= 0L) {

                    World w = null;

                    if(players.contains(i + ".loc.world")) w = Bukkit.getWorld(players.getString(i + ".loc.world"));

                    if(w == null) w = Bukkit.getWorlds().get(0);

                    Location loc = w.getSpawnLocation();

                    if(players.contains(i + ".loc.x")) {

                        loc = new Location(w, players.getDouble(i + ".loc.x"), players.getDouble(i + ".loc.y"),
                                players.getDouble(i + ".loc.z"), (float) players.getLong(i + ".loc.yaw"),
                                (float) players.getLong(i + ".loc.pitch"));
                    } else System.out.print("[DEBUG] Error location prison out for : " + p.getName());

                    p.teleport(loc);

                    p.setHealth(players.getDouble(i + ".vie"));
                    p.setFoodLevel(players.getInt(i + ".food"));
                    p.setSaturation(players.getInt(i + ".saturation"));
                    p.setLevel(players.getInt(i + ".lvl"));
                    p.setExp((float) players.getDouble(i + ".exp"));

                    ItemStack[] armor = new ItemStack[4];
                    ItemStack[] content = new ItemStack[36];

                    for(int x = 0; x <= 3; x++) armor[x] = players.getItemStack(i + ".armor." + x);

                    for(String key : players.getKeys(true)) {

                        String[] k = key.split("[.]");

                        if(k.length >= 3 && k[0].equals(i.toString()) && k[1].equals("stuff")) {

                            content[Integer.parseInt(k[2])] = players.getItemStack(i + ".stuff." + k[2]);
                        }
                    }

                    p.getInventory().setArmorContents(armor);
                    p.getInventory().setContents(content);

                    int nb = players.getInt(i + ".nb");

                    players.set(i + "", null);
                    players.set(i + ".nb", nb);

                    cmP.saveConfig("players.yml");

                    if(prison.contains(p)) prison.remove(p);
                    p.sendMessage(Langs.unjailedMessage.replace("$player", p.getName()));

                    Bukkit.getScheduler().cancelTask(task.get(p));
                }

                int min = (int) (time.longValue() - time.longValue() % 60000L) / 60000;
                if(time.longValue() % 60000L != 0L) min++;
	
	       /* if(min > 1) {
	        
	        	IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" +
	        			Langs.minuteJailed.replace("$player", p.getName())
	        			.replace("$time", min + "")
	        			.replace("$raison", raison) + "\"}");
	
	        	((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(cbc, (byte) 2));
	        }
	        else {
	        	
	        	int sec = (int)(time.longValue() - time.longValue() % 1000L) / 1000;
	        	if (time.longValue() % 1000L != 0L) sec++;
	        	
	        	IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + 
	        			 Langs.secondJailed.replace("$player", p.getName())
	        			 .replace("$time", sec + "")
	        			 .replace("$raison", raison) + "\"}");
	
	        	 ((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(cbc, (byte) 2));
	
	        	 if (sec <= 3) p.playSound(p.getLocation(), Sound.ORB_PICKUP, 100.0F, 1.0F);
	        }*/

                Location l = p.getLocation();

                if((yaw.containsKey(p)) && (pitch.containsKey(p))) {

                    if(yaw.get(p) == l.getYaw() && pitch.get(p) == l.getPitch()) {

                        if(tickAFK.containsKey(p)) {

                            int newAFK = ((Integer) tickAFK.get(p)).intValue() + 10;

                            if(1200 * config.getInt("afk") == newAFK) {

                                if(p.isOnline()) Bukkit.broadcastMessage(Langs.kickAFK.replace("$player", p.getName()));
                                p.kickPlayer(Langs.quitAFK.replace("$player", p.getName()));
                            } else tickAFK.put(p, Integer.valueOf(newAFK));
                        } else tickAFK.put(p, Integer.valueOf(10));
                    } else tickAFK.put(p, Integer.valueOf(10));
                } else tickAFK.put(p, Integer.valueOf(10));

                yaw.put(p, l.getYaw());
                pitch.put(p, l.getPitch());

                players.set(i + ".jail", Long.valueOf(time.longValue() - (1000L * 60)));
                cmP.saveConfig("players.yml");
            }
        }, 0L, 60 * 20L));

        task2.put(p, Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new BukkitRunnable() {
            @Override
            public void run() {

                Long time = Long.valueOf(players.getLong(i + ".jail"));

                int min = (int) (time - time % 60000L) / 60000;
                if(time % 60000L != 0L) min++;

                IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" +
                        Langs.minuteJailed.replace("$player", p.getName())
                                .replace("$time", min + "")
                                .replace("$raison", raison) + "\"}");

                ((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(cbc, (byte) 2));
            }
        }, 10L, 10L));
	
	    p.sendMessage(Langs.jailedMessage.replace("$player", p.getName()).replace("$time", min + "").replace("$raison", 
	      raison));
	}
}