-injars 'SandboxCore.jar'
-outjars 'SandboxCoreObfuscated.jar'
 
-libraryjars <java.home>/lib/rt.jar
-libraryjars lib/
 
-dontshrink
-dontoptimize
-useuniqueclassmembernames
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,LocalVariable*Table,*Annotation*,Synthetic,EnclosingMethod
-ignorewarnings
 
 
-keep,allowshrinking class net.IceRhal.SandboxCore.Main
 
# Keep names - Native method names. Keep all native class/method names.
-keepclasseswithmembers,includedescriptorclasses,allowshrinking class * {
    native <methods>;

    }